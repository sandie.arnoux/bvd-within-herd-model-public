
# BVD within-herd model (cow-calf, vaccination)
# =============================================
# 
# Contributors and contact:
# -------------------------
# 
#     - Sandie Arnoux (sandie.arnoux@inrae.fr)
#     - Pauline Ezanno (pauline.ezanno@inrae.fr)
# 
#     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
# 
# 
# How to cite:
# ------------
# 
#     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
#     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
#     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
# 
# 
# License:
# --------
# 
#    Copyright 2012 INRAE
# 
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#        http://www.apache.org/licenses/LICENSE-2.0
# 
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

# coding: utf-8
#! /usr/bin/env python

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from argparse import ArgumentParser
from argparse import RawTextHelpFormatter

import libBVDV_intra_beef_cattle as mod
from ModelInterface import ModelInterface
from Parameters import Parameters
from Outputs import EconomicOutputs
from Outputs import Impact
from Outputs import Gain

def getVaccinesValues():
    return pd.read_csv("../data/vaccines.csv", index_col=["id"])

if __name__ == "__main__":
    parser = ArgumentParser(description = "Simulate the spread of Bovine Viral Diarrhoea (BVD) virus within a cow-calf herd", formatter_class = RawTextHelpFormatter)
    parser.add_argument("-o", "--option", type=str, default=1, help="1. basic scenario : impact and gain\n")
    args = parser.parse_args()
    option = args.option
    resultsDirPath = "../results/"
    iMI = ModelInterface()
    plt.rcParams.update({'font.size': 12})

    if "1" in option:
        model = mod.CommunicateWithGUI()
        iImpact = Impact()
        iGain = Gain()
        df = getVaccinesValues()
        vaccNum = 1 #Bovilis
        caseNum = 2 #Charolais
        runsNb = 1000
        yearsNb = 8
        Kext = 0.00025
        iParams = Parameters(caseNum)
        iParams.yearsNb = yearsNb
        iParams.runsNb = runsNb
        #run model without BVD
        iMI.runModel(model, iParams)
        iEcoOut = EconomicOutputs(iParams)
        iEcoOut.extract(model)
        aEbitdarWithoutDisease = iEcoOut.computeEbitdar()
        #run model with BVD (introduced by neighborhood contacts)
        iParams.KextStartYearNum = 0
        iParams.Kext = Kext
        iMI.runModel(model, iParams)
        iEcoOut = EconomicOutputs(iParams)
        iEcoOut.extract(model)
        aEbitdarWithDisNoStrat = iEcoOut.computeEbitdar()
        aImpactNoStrat = iImpact.compute(aEbitdarWithoutDisease, aEbitdarWithDisNoStrat)
        #run model with BVD and vaccination
        iParams.vaccScenario = 1
        iParams.vaccStartYearNum = 0
        iParams.vaccEndYearNum = yearsNb
        iParams.vaccEfficiencyAgainstInfection = df.loc[vaccNum]["vaccine_efficiency"]
        iParams.vaccEfficiencyAgainstSymptoms = df.loc[vaccNum]["protection_vertical"]
        iParams.partialImmunityWeeksNb = df.loc[vaccNum]["partial_immunity_duration"]
        iParams.primoVaccDoseNb = df.loc[vaccNum]["nb_primo_vacc"]
        iParams.realVaccFullImmunityWeeksNb = df.loc[vaccNum]["full_immunity_duration"]
        iParams.vaccDoseCost = df.loc[vaccNum]["cost_unit_per_cow"]
        iMI.runModel(model, iParams)
        iEcoOut = EconomicOutputs(iParams)
        iEcoOut.extract(model)
        aEbitdarWithDisWithStrat = iEcoOut.computeEbitdar()
        aImpactWithStrat = iImpact.compute(aEbitdarWithoutDisease, aEbitdarWithDisWithStrat)
        meanVaccCostPerYear = iGain.computeVaccinationCost(iParams)
        #compare without/with vaccination
        aGain = iGain.compute(iParams, aImpactNoStrat, aImpactWithStrat)
        #draw figure
        x = range(yearsNb+1)
        fig = plt.figure()
        ax = plt.subplot(1, 1, 1)
        aImpactPerCow = aImpactNoStrat / (iParams.breedHeifNb + iParams.breedCowsNb)
        aGainPerCowPerYear = aGain / meanVaccCostPerYear
        plt.plot(x, np.percentile(aImpactPerCow, 50, axis=1), label = "impact per breeding female if no vaccination", c = "red")
        plt.fill_between(x, np.percentile(aImpactPerCow, 25, axis=1), np.percentile(aImpactPerCow, 75, axis=1), color="red", alpha=0.4)
        plt.plot(x, np.percentile(aGainPerCowPerYear, 50, axis=1), label = "reward per euro invested per vaccinated animal", c = "green")
        plt.fill_between(x, np.percentile(aGainPerCowPerYear, 25, axis=1), np.percentile(aGainPerCowPerYear, 75, axis=1), color="green", alpha=0.4)
        ax.set_xticks(range(0, yearsNb+1, 2))
        plt.axhline(y=0, color="black", linestyle="--", alpha=0.4)
        plt.xlabel("year")
        plt.ylabel("euro per cow")
        plt.xlim(0, yearsNb)
        plt.legend(bbox_to_anchor=(0.95, 1.18), framealpha=0.)
        fig.tight_layout()
        plt.savefig("{}/impactGain.png".format(resultsDirPath))
        plt.close("all")
