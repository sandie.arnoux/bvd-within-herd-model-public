
# BVD within-herd model (cow-calf, vaccination)
# =============================================
# 
# Contributors and contact:
# -------------------------
# 
#     - Sandie Arnoux (sandie.arnoux@inrae.fr)
#     - Pauline Ezanno (pauline.ezanno@inrae.fr)
# 
#     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
# 
# 
# How to cite:
# ------------
# 
#     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
#     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
#     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
# 
# 
# License:
# --------
# 
#    Copyright 2012 INRAE
# 
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#        http://www.apache.org/licenses/LICENSE-2.0
# 
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import numpy as np

class EconomicOutputs:

    def __init__(self, iParams = None):
        self.iParams = iParams

    def extract(self, model):
        lEntities = []
        for entity in range(0, 82):
            lEntities.append(model.get_name_output(entity))

        setattr(self, "aEmptyHeifNb", np.array(model.get_data(lEntities.index("Empty_Heif")), dtype=float))
        setattr(self, "aEmptyCowsNb", np.array(model.get_data(lEntities.index("Empty_Cows")), dtype=float))
        setattr(self, "aSoldMaleGrassersNb", np.array(model.get_data(lEntities.index("Sell_Male_Grassers")), dtype=float))
        setattr(self, "aSoldFemaleGrassersNb", np.array(model.get_data(lEntities.index("Sell_Female_Grassers")), dtype=float))
        setattr(self, "aSoldHeifersNb", np.array(model.get_data(lEntities.index("Sell_Heifers")), dtype=float))
        setattr(self, "aSoldFattenedFemalesNb", np.array(model.get_data(lEntities.index("Sell_Fattened_Females_At_Weaning")), dtype=float))
        setattr(self, "aPurchasedCalvesNb", np.array(model.get_data(lEntities.index("Purchase_Calves")), dtype=float))
        setattr(self, "aPurchasedFemalesNb", np.array(model.get_data(lEntities.index("Purchase_Pregnant_Females")), dtype=float))

    def computeEbitdar(self):
        #EBITDAR = Earnings Before Interest Taxes Depreciation Amortization and Rent
        #In this case, not really an EBITDAR, because it's a delta of production cost (allowed because herd sizes equivalent), no global production cost
        #prices
        aSoldMaleGrassersCost = self.aSoldMaleGrassersNb * (self.iParams.malesSoldAtAutumnProp * self.iParams.maleSoldAtAutumnCost \
                                                          + self.iParams.malesSoldAtWinterProp * self.iParams.maleSoldAtWinterCost \
                                                          + self.iParams.malesSoldAtSpringProp * self.iParams.maleSoldAtSpringCost)
        aSoldFemaleGrassersCost = self.aSoldFemaleGrassersNb * (self.iParams.femalesSoldAtAutumnProp * self.iParams.femaleSoldAtAutumnCost \
                                                              + self.iParams.femalesSoldAtWinterProp * self.iParams.femaleSoldAtWinterCost \
                                                              + self.iParams.femalesSoldAtSpringProp * self.iParams.femaleSoldAtSpringCost)
        aSoldHeifersCost = self.aSoldHeifersNb * self.iParams.fatHeiferSaleCost
        aSoldFemalesForCullingCost = (self.aEmptyHeifNb + self.aEmptyCowsNb + self.aSoldFattenedFemalesNb) * self.iParams.culledCowSaleCost
        aPurchasedCalvesCost = self.aPurchasedCalvesNb * self.iParams.calfPurchaseCost
        aPurchasedFemalesCost = self.aPurchasedFemalesNb * self.iParams.gestCowPurchaseCost
        #kg
        aSoldMaleGrassersKg = self.aSoldMaleGrassersNb * (self.iParams.malesSoldAtAutumnProp * self.iParams.maleSoldAtAutumnWeight \
                                                        + self.iParams.malesSoldAtWinterProp * self.iParams.maleSoldAtWinterWeight \
                                                        + self.iParams.malesSoldAtSpringProp * self.iParams.maleSoldAtSpringWeight)
        aSoldFemaleGrassersKg = self.aSoldFemaleGrassersNb * (self.iParams.femalesSoldAtAutumnProp * self.iParams.femaleSoldAtAutumnWeight \
                                                            + self.iParams.femalesSoldAtWinterProp * self.iParams.femaleSoldAtWinterWeight \
                                                            + self.iParams.femalesSoldAtSpringProp * self.iParams.femaleSoldAtSpringWeight)
        aSoldHeifersKg = self.aSoldHeifersNb * self.iParams.fatHeiferSaleWeight
        aSoldFemalesForCullingKg = (self.aEmptyHeifNb + self.aEmptyCowsNb + self.aSoldFattenedFemalesNb) * self.iParams.culledCowSaleWeight
        aPurchasedCalvesKg = self.aPurchasedCalvesNb * self.iParams.calfPurchaseWeight
        aPurchasedFemalesKg = self.aPurchasedFemalesNb * self.iParams.gestCowPurchaseWeight
        #production cost
        aProductionCostDelta = self.iParams.animalsCost / float(self.iParams.totalMeatProd) \
                                                        * \
                             ( (aSoldMaleGrassersKg + aSoldFemaleGrassersKg + aSoldHeifersKg + aSoldFemalesForCullingKg) \
                             - (aPurchasedCalvesKg + aPurchasedFemalesKg) )
        #ebitdar
        aEbitdar = (aSoldMaleGrassersCost + aSoldFemaleGrassersCost + aSoldHeifersCost + aSoldFemalesForCullingCost) \
                 - (aPurchasedCalvesCost + aPurchasedFemalesCost + aProductionCostDelta)
        return aEbitdar #should be > 0, we except to have more sales than purchases



def sortBySumPerColumn(a):
    return a[:, np.argsort(a.sum(axis=0))]

class Impact:

    def compute(self, aEbitdarWithoutDisease, aEbitdarWithDisease):
        aEBDNoDisSorted = sortBySumPerColumn(aEbitdarWithoutDisease)
        aEBDWithDisSorted = sortBySumPerColumn(aEbitdarWithDisease)
        aImpact = aEBDWithDisSorted - aEBDNoDisSorted
        aImpact[aImpact>0] = 0
        return aImpact



class Gain:

    def computeVaccinationCost(self, iParams):
        #The first time an animal is vaccinated, sometimes there is more than 1 dose => primo vaccination
        costFirstYear = (iParams.breedHeifNb * iParams.vaccHeifCoverage + iParams.breedCowsNb * iParams.vaccCowsCoverage) * iParams.primoVaccDoseNb * iParams.vaccDoseCost
        costPerYear = (iParams.breedHeifNb * iParams.vaccHeifCoverage * iParams.primoVaccDoseNb + iParams.breedCowsNb * iParams.vaccCowsCoverage) * iParams.vaccDoseCost
        meanVaccCostPerYear = (costFirstYear + costPerYear * (iParams.yearsNb-1)) / float(iParams.yearsNb) + iParams.vaccVetoCost - iParams.vaccSubsidiesAmount
        return meanVaccCostPerYear

    def compute(self, iParams, aImpactNoStrat, aImpactWithStrat):
        if iParams.vaccScenario:
            aImpactWithStratCost = aImpactWithStrat - self.computeVaccinationCost(iParams)
        else:
            raise Exception("Can't compute a gain, because no vaccination and no other control strategy is implemented...")
        return aImpactWithStratCost - aImpactNoStrat
