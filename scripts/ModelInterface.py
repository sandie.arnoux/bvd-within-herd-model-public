
# BVD within-herd model (cow-calf, vaccination)
# =============================================
# 
# Contributors and contact:
# -------------------------
# 
#     - Sandie Arnoux (sandie.arnoux@inrae.fr)
#     - Pauline Ezanno (pauline.ezanno@inrae.fr)
# 
#     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
# 
# 
# How to cite:
# ------------
# 
#     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
#     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
#     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
# 
# 
# License:
# --------
# 
#    Copyright 2012 INRAE
# 
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#        http://www.apache.org/licenses/LICENSE-2.0
# 
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import os

class ModelInterface:

    def runModel(self, model, iParams):
        model.reset()
        model.set_params_calves(iParams.deadCalvesTotalProp, iParams.twinningRate, iParams.heifersFertilityRate, iParams.cowsFertilityRate)
        model.set_params_sales(iParams.malesSoldAtAutumnProp, iParams.malesSoldAtWinterProp, iParams.malesSoldAtSpringProp, \
                               iParams.femalesSoldAtAutumnProp, iParams.femalesSoldAtWinterProp, iParams.femalesSoldAtSpringProp)
        model.set_params_epidemio(iParams.probaOfPDeathAtBirth, iParams.mu_P, iParams.beta_T, iParams.beta_P, iParams.beta, iParams.rate_M_S, iParams.rate_T_R, iParams.probaAbortionIfRa, iParams.probaAbortionIfRb, iParams.probaPbirthIfRb, iParams.probaMbirthIfRb, iParams.probaRbirthIfRb)
        model.set_params_contacts(iParams.Kext, iParams.isContactYoungHeifBreedHeif, iParams.isContactYoungHeifCows, iParams.isContactBreedHeifCows, \
                                  iParams.isContactYoungHeifHerdOutside, iParams.isContactBreedHeifHerdOutside, iParams.isContactCalvesCowsHerdOutside, \
                                  iParams.KextStartYearNum)
        model.set_params_vaccination(iParams.vaccStartYearNum, iParams.vaccEndYearNum, iParams.vaccScenario, \
                                     iParams.fullImmunityWeeksNb, int(iParams.partialImmunityWeeksNb), \
                                     iParams.vaccHeifCoverage, iParams.vaccCowsCoverage, \
                                     float(iParams.vaccEfficiencyAgainstInfection), float(iParams.vaccEfficiencyAgainstSymptoms))
        model.create_a_new_simulation(iParams.runsNb, iParams.yearsNb, iParams.weaningDate, \
                                      iParams.heifBreedStartDate, iParams.breedEndDate, \
                                      iParams.indoorStartDate, iParams.pastureStartDate, \
                                      int(iParams.breedHeifNb), int(iParams.breedCowsNb), int(iParams.heifForSaleNb), float(iParams.riskFactor))
        
        model.run()
