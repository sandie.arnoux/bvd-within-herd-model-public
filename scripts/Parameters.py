
# BVD within-herd model (cow-calf, vaccination)
# =============================================
# 
# Contributors and contact:
# -------------------------
# 
#     - Sandie Arnoux (sandie.arnoux@inrae.fr)
#     - Pauline Ezanno (pauline.ezanno@inrae.fr)
# 
#     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
# 
# 
# How to cite:
# ------------
# 
#     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
#     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
#     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
# 
# 
# License:
# --------
# 
#    Copyright 2012 INRAE
# 
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
# 
#        http://www.apache.org/licenses/LICENSE-2.0
# 
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import pandas as pd

class Parameters:

    def __init__(self, caseNum):
        self.probaOfPDeathAtBirth = 0.0666
        self.mu_P = 0.0019                  #1. - pow((1. - self.probaOfPDeathPerYear), (1. / 365.))
        self.beta_T = 0.03
        self.beta_P = 0.5
        self.beta = 0.1
        self.rate_M_S = 1. / 150.
        self.rate_T_R = 1. / 5.
        self.probaAbortionIfRa = 0.8
        self.probaAbortionIfRb = 0.2
        self.probaPbirthIfRb = 0.75
        self.probaMbirthIfRb = 0.025
        self.probaRbirthIfRb = 0.025

        self.yearsNb = 10
        self.runsNb = 1000

        self.isContactYoungHeifBreedHeif = False
        self.isContactYoungHeifCows = False
        self.isContactBreedHeifCows = False
        self.isContactYoungHeifHerdOutside = True
        self.isContactBreedHeifHerdOutside = True
        self.isContactCalvesCowsHerdOutside = True

        self.Kext = 0.
        self.KextStartYearNum = -1

        self.vaccScenario = 0
        self.vaccStartYearNum = -1
        self.vaccEndYearNum = -1
        self.fullImmunityWeeksNb = 52
        self.partialImmunityWeeksNb = 26
        self.vaccHeifCoverage = 1.
        self.vaccCowsCoverage = 1.
        self.vaccEfficiencyAgainstInfection = 1.
        self.vaccEfficiencyAgainstSymptoms = 1.
        self.primoVaccDoseNb = 1
        self.vaccDoseCost = 5.
        self.vaccVetoCost = 0.
        self.vaccSubsidiesAmount = 0.
        self.realVaccFullImmunityWeeksNb = 52

        dfCases = self.getTypicalCaseValues()
        self.heifForSaleNb = dfCases["nb_heif_for_selling"][caseNum]
        self.breedHeifNb = dfCases["nb_heifers_breed"][caseNum]
        self.breedCowsNb = dfCases["nb_cows_breed"][caseNum]
        self.riskFactor = dfCases["productivity_level"][caseNum]

        self.weaningDate = str(dfCases["weaning"][caseNum])
        self.heifBreedStartDate = str(dfCases["breeding_start"][caseNum])
        self.breedEndDate = str(dfCases["breeding_end"][caseNum])
        self.indoorStartDate = str(dfCases["inside"][caseNum])
        self.pastureStartDate = str(dfCases["outside"][caseNum])

        self.deadCalvesTotalProp = dfCases["proba_death_ca"][caseNum]
        self.deadCalvesAfterBirthProp = self.deadCalvesTotalProp * 0.75
        self.deadCalvesAtBirthProp = self.deadCalvesTotalProp * 0.25
        self.mu_Ca = 1. - pow((1. - self.deadCalvesAfterBirthProp), (1. / 190.))
        self.twinningRate = dfCases["twinning"][caseNum]
        self.heifersFertilityRate = dfCases["fertility_h"][caseNum]
        self.cowsFertilityRate = dfCases["fertility_c"][caseNum]

        self.malesSoldAtAutumnProp = dfCases["proportion_autumn_males"][caseNum]
        self.malesSoldAtWinterProp = dfCases["proportion_winter_males"][caseNum]
        self.malesSoldAtSpringProp = dfCases["proportion_spring_males"][caseNum]
        self.femalesSoldAtAutumnProp = dfCases["proportion_autumn_females"][caseNum]
        self.femalesSoldAtWinterProp = dfCases["proportion_winter_females"][caseNum]
        self.femalesSoldAtSpringProp = dfCases["proportion_spring_females"][caseNum]

        self.maleSoldAtAutumnCost = dfCases["cost_sale_autumn_broutards"][caseNum]
        self.maleSoldAtWinterCost = dfCases["cost_sale_winter_broutards"][caseNum]
        self.maleSoldAtSpringCost = dfCases["cost_sale_spring_broutards"][caseNum]
        self.femaleSoldAtAutumnCost = dfCases["cost_sale_autumn_broutardes"][caseNum]
        self.femaleSoldAtWinterCost = dfCases["cost_sale_winter_broutardes"][caseNum]
        self.femaleSoldAtSpringCost = dfCases["cost_sale_spring_broutardes"][caseNum]
        self.fatHeiferSaleCost = dfCases["cost_sale_fat_heif"][caseNum]
        self.culledCowSaleCost = dfCases["cost_sale_cull_cows"][caseNum]
        self.calfPurchaseCost = dfCases["cost_purchase_replace_calf"][caseNum]
        self.gestCowPurchaseCost = dfCases["cost_purchase_gestating_cows"][caseNum]
        self.maleSoldAtAutumnWeight = dfCases["weight_sale_autumn_broutards"][caseNum]
        self.maleSoldAtWinterWeight = dfCases["weight_sale_winter_broutards"][caseNum]
        self.maleSoldAtSpringWeight = dfCases["weight_sale_spring_broutards"][caseNum]
        self.femaleSoldAtAutumnWeight = dfCases["weight_sale_autumn_broutardes"][caseNum]
        self.femaleSoldAtWinterWeight = dfCases["weight_sale_winter_broutardes"][caseNum]
        self.femaleSoldAtSpringWeight = dfCases["weight_sale_spring_broutardes"][caseNum]
        self.fatHeiferSaleWeight = dfCases["weight_sale_fat_heif"][caseNum]
        self.culledCowSaleWeight = dfCases["weight_sale_cull_cows"][caseNum]
        self.calfPurchaseWeight = dfCases["weight_purchase_replace_calf"][caseNum]
        self.gestCowPurchaseWeight = dfCases["weight_purchase_gestating_cows"][caseNum]
        self.animalsCost = dfCases["animals_cost"][caseNum]
        self.totalMeatProd = dfCases["total_meat_prod"][caseNum]
    
    def getTypicalCaseValues(self):
        return pd.read_csv("../data/farmingSystems_2014.csv", index_col=["id"], sep="\t")
