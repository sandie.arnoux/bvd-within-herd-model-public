
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "../includes.h"

namespace model {

struct Period{
	int start;
	int end;

	Period(){
        start = 0;
        end = 0;
    }
	Period(const unsigned int start, const unsigned int end){
		this->start = start;
		this->end = end;
	}
	~Period(){}

    void add(const int x){ start += x; end += x; }
    void divide(const int x){ start /= x; end /= x; }
    void multiply(const int x){ start *= x; end *= x; }
    void modulo(const int x){ start %= x; end %= x; }
};

class Herd_Calendar {
private:
    int weaning_in_days;

	void set_duration_of_full_immunity_for_vaccinated_animals(){
        Parameters& iParams = Parameters::getInstance();
		// calculation of the date when vaccinated females change from state V1 to V2
		progressive_loss_of_immunity = (repro_cows.start + iParams.fullImmunityWeeksNb) % iParams.weeksNbPerYear;

		// calculation of the duration of the existence of the bred heifers group
		int how_much_time_females_stay_in_bred_heifers_group = (indoor_period.start + iParams.weeksNbPerYear) - repro_heifers.start;

		// if the duration of the existence of the bred heifers group is > of the duration of the full immunity
		// (= time in state V1), it means that bred heifers will change from state V1 to V2 before becoming cows.
		is_OK_for_bred_heifers = iParams.fullImmunityWeeksNb <= how_much_time_females_stay_in_bred_heifers_group;
	}

public:
	enum list_of_months{
		Jan_=1, Feb_, Mar_, Apr_, May_, Jun_, Jul_, Aug_, Sep_, Oct_, Nov_, Dec_
	};

    Period repro_heifers;
    Period repro_cows;
    Period calving_period;
    Period indoor_period;
    Period purchase_calves;
    int weaning;
    int sell_empty;
    int replacement_bull;
    int progressive_loss_of_immunity;
    bool is_OK_for_bred_heifers;

    vector<int*> v_dates = {&weaning, &sell_empty, &replacement_bull, &progressive_loss_of_immunity,
    						&repro_heifers.start, &repro_heifers.end, &repro_cows.start, &repro_cows.end,
    						&calving_period.start, &calving_period.end, &indoor_period.start, &indoor_period.end,
    						&purchase_calves.start, &purchase_calves.end};

	Herd_Calendar(){
        Parameters& iParams = Parameters::getInstance();
        date heifBreedStartDate = from_undelimited_string(iParams.heifBreedStartDate);
        date breedEndDate = from_undelimited_string(iParams.breedEndDate);

		// start(end) repro + 285 - 365 = start(end)_repro - 80
		date start_calvings = heifBreedStartDate - date_duration(80);
		date end_calvings = breedEndDate - date_duration(80);

        //TODO: to uncomment ?!? because "bred_heifers->calvings()" is disabled !
		//if( start_indoor.day_of_year() > start_calvings.day_of_year() )
		//	throw std::invalid_argument("indoor period must start before the beginning of the calvings");

		// weaning = week 51 = end of a year
		weaning = 51;
		weaning_in_days = from_undelimited_string(iParams.weaningDate).day_of_year();

		// reproduction period for heifers and cows
		int start = convert_to_week_according_to_weaning(heifBreedStartDate);
		int end = convert_to_week_according_to_weaning(breedEndDate);
		repro_heifers = Period(start, end);
		repro_cows = Period(start + iParams.weeksNbBetweenHeifCowsBreedStarts, end);

		// calvings period (= reproduction period + gestating time modulo 365 days)
		int start_calvings_week = convert_to_week_according_to_weaning(start_calvings);
		end = convert_to_week_according_to_weaning(end_calvings);
		calving_period = Period(start_calvings_week, end);

		// purchase calves period (= calving period + few months)
		purchase_calves = Period(start_calvings_week, end + iParams.weeksNbAfterCalvingEndToPurchaseCalf);

		// indoor period
		int start_indoor_week = convert_to_week_according_to_weaning(from_undelimited_string(iParams.indoorStartDate));
		end = convert_to_week_according_to_weaning(from_undelimited_string(iParams.pastureStartDate));
		indoor_period = Period(start_indoor_week, end);

		if (start_indoor_week > start_calvings_week)
			throw std::invalid_argument("indoor period must start before the beginning of the calvings");

		// selling empty females => 100 days after beginning of indoor period
		sell_empty = start_indoor_week + iParams.weeksNbAfterIndoorStartToSellEmpty;

		// replacement of a bull
		replacement_bull = convert_to_week_according_to_weaning(1, Nov_);

		// beginning of the progressive loss of immunity for vaccinated females
		set_duration_of_full_immunity_for_vaccinated_animals();
	}

	virtual ~Herd_Calendar(){}

	void plus_one_year(){
		for(int* date : v_dates) *date += Parameters::getInstance().weeksNbPerYear;
	}

	void shift_to_year_0(){
		for(int* date : v_dates) *date %= Parameters::getInstance().weeksNbPerYear;
	}

	int convert_to_week_according_to_weaning(const int day, const int month, const int year=0) {
		return convert_to_week_according_to_weaning(date(2009, month, day), year);
	}

    int convert_to_week_according_to_weaning(const date date_to_convert, const int year=0) {
        Parameters& iParams = Parameters::getInstance();
    	int day_of_year = date_to_convert.day_of_year();
    	int shifted_day = day_of_year - weaning_in_days;
		if(shifted_day < 0) shifted_day += 365;
		int date_in_weeks = (shifted_day / iParams.daysNbPerWeek) + year * iParams.weeksNbPerYear;
		return  date_in_weeks;
    }

    void print_dates(){
    	cout << "dates of the calendar: " << endl;
    	cout << "weaning: " << weaning << endl;
    	cout << "replacement of a bull: " << replacement_bull << endl;
    	cout << "sell empty females: " << sell_empty << endl;
    	cout << "indoor period: " << indoor_period.start << " -> " << indoor_period.end << endl;
    	cout << "calving period: " << calving_period.start << " -> " << calving_period.end << endl;
    	cout << "breeding period (heifers): " << repro_heifers.start << " -> " << repro_heifers.end << endl;
    	cout << "breeding period (cows): " << repro_cows.start << " -> " << repro_cows.end << endl;
    	cout << "progressive loss of immunity: " << progressive_loss_of_immunity << endl;
    	cout << "\n" << endl;
    }
};

}
