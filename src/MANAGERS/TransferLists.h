
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "Transfer.h"
#include "GenerateCalvingDates.h"

namespace model {

// Structure inherited by TransferList and AbstractGestatingGroup classes
struct Common_For_Lists{
	const static int nb_lists = 8;
	enum enum_lists{
		SS_, TR_, RM_, RR_, RP_, PP_, V1M_, V2M_
	};

	// additional "states" that are not linked to a list
	const static int RX_ = nb_lists + 1;		// undetermined state of foetus after infection
	const static int DD_ = nb_lists + 2;		// dead females

	typedef vector<int> list_calving_dates;
	typedef array<vector<int>, nb_lists> array_lists;

    /*! Selects one date randomly in a given list.\n
     *  Then, the value is stored in the field \em date.\n
     *  Finally, the selected date is removed from the list.
     */
	int select_and_remove_one_date_from_list(list_calving_dates& list){
		int date = 0;
		if(list.size() > 0){
			list_calving_dates::iterator ms_it;
			ms_it = list.begin();
			advance( ms_it, uniform_int(0, list.size()) );
			date = *ms_it;
			list.erase(ms_it);
		}
		return date;
	}
};


class Transfer_Lists: public model::Transfer, public model::Common_For_Lists {
private:
	array_lists& lists;
	unsigned int& nb_deaths;
	unsigned int nb_abortions = 0;

	Generate_Calving_Dates* date_generator;
	boost::random::discrete_distribution<> dist_Rb_females;
	enum e_Rb_females{
		Rb_Abortion, Rb_Giving_Birth_To_M_Calf, Rb_Giving_Birth_To_P_Calf, Rb_Giving_Birth_To_R_Calf
	};

    /*! Transfers N calving dates from list[TR_] to list[RM_] or list[RR_] or list[RP_]
     *  \attention "empty" dates from list TR_ are transfered to list RM_ */
    void transfer_from_list_TR_to_list_RX(const unsigned int nb_transfers, const int week_recovery){
        Parameters& iParams = Parameters::getInstance();
    	int week_infection = week_recovery - 1; 	// duration of infection = one week
    	int date, delay_infection_start_pregnancy;
    	for (unsigned int q=0; q<nb_transfers; q++){
			date = select_and_remove_one_date_from_list( lists[TR_ ] );

			if (date > iParams.emptyDate){
				// start_pregnancy = calving_date - gestating_time
				int start_pregnancy = date - iParams.gestatingDuration;
				// delta = infection date - start pregnancy OR -10 if not yet pregnant
				delay_infection_start_pregnancy = (week_infection >= start_pregnancy) ? week_recovery - start_pregnancy : -10;
			} else {
				delay_infection_start_pregnancy = -10;
			}

			// if delta = -10 --> not pregnant
			if (delay_infection_start_pregnancy == -10) lists[RM_].push_back(date);

			// if 0 < delta <= 41 days --> Ra
			if (delay_infection_start_pregnancy >= 0 && delay_infection_start_pregnancy <= iParams.lastGestatingWeekNumOfRa){
				if(bernoulli(iParams.probaAbortionIfRa)){
					abortion(week_infection);
				}else{
					lists[RM_].push_back(date);
				}
			}

			// if 42 < delta <= 150 days --> Rb
			if (delay_infection_start_pregnancy > iParams.lastGestatingWeekNumOfRa && delay_infection_start_pregnancy <= iParams.lastGestatingWeekNumOfRb){
				int i = dist_Rb_females(iParams.gen);
				switch(i){
					case Rb_Abortion:
						abortion(week_infection);
						break;
					case Rb_Giving_Birth_To_M_Calf:
						lists[RM_].push_back(date);
						break;
					case Rb_Giving_Birth_To_P_Calf:
						lists[RP_].push_back(date);
                        Explore::getInstance().addIfPasture(Explore::eNewRPAtPasture, 1);
						break;
					case Rb_Giving_Birth_To_R_Calf:
						lists[RR_].push_back(date);
						break;
				}
			}

			// if 151 < delta <= 285 days --> Rc
			if(delay_infection_start_pregnancy > iParams.lastGestatingWeekNumOfRb && delay_infection_start_pregnancy <= iParams.lastGestatingWeekNumOfRc){
				lists[RR_].push_back(date);
			}
		}
    }

    void abortion(const int week){
        nb_abortions++;
        int date = date_generator->get_calving_date_after_abortion(week);
        lists[RM_].push_back(date);
    }

public:
	Transfer_Lists(array_lists& lists, unsigned int& nb_deaths, Generate_Calving_Dates* date_generator) :
			lists(lists), nb_deaths(nb_deaths), date_generator(date_generator){
	    Parameters& iParams = Parameters::getInstance();
		dist_Rb_females = boost::random::discrete_distribution<>{iParams.probaAbortionIfRb, iParams.probaMbirthIfRb, iParams.probaPbirthIfRb, iParams.probaRbirthIfRb};
	}

	virtual ~Transfer_Lists(){}

	unsigned int get_nb_abortions() const{
		return nb_abortions;
	}

	void reset_nb_abortions(){
		nb_abortions = 0;
	}

	void calculate_and_make_transfers(const int week){
		m_transfers.setZero();
		for(unsigned int source : v_sources)
			if(lists[source].size() > 0) calculate_multinomial(source, lists[source].size());

		unsigned int nb_transfers = 0;
		for(unsigned int source : v_sources){
			if(lists[source].size() > 0){
				for(unsigned int destination : map_destinations[source]){
					nb_transfers = m_transfers(source, destination);
					switch(destination){
						case RX_:
							transfer_from_list_TR_to_list_RX(nb_transfers, week);
							break;
						case DD_:
							nb_deaths += nb_transfers;
							for(unsigned int q=0; q<nb_transfers; q++)
								select_and_remove_one_date_from_list( lists[source] );
							break;
						default:
							for(unsigned int q=0; q<nb_transfers; q++)
								lists[destination].push_back( select_and_remove_one_date_from_list( lists[source] ));
							break;
					}
				}
			}
		}
	}
};

}
