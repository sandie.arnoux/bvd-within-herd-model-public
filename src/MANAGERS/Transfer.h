
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "../includes.h"

namespace model {

class Transfer {
private:
    void calculate_probabilities(unsigned int source){
    	// proba calculation
		// sum(rates) + normalization coefficient calculation
		double sum_rates = 0.;
		for(unsigned int dest : map_destinations[source]) sum_rates += m_rates(source, dest);
        //See Breto publication on how to avoid event ordering by using multinomial
        double coeff_normalization = (sum_rates>1.e-10) ? (1. - exp(-sum_rates * Parameters::getInstance().daysNbPerTimeStep)) / sum_rates : 0.;

		// proba calculation (included the proba of remaining in the same compartment)
		double sum_probas = 0.;
		for(unsigned int dest : map_destinations[source]){
			m_probabilities(source, dest) = coeff_normalization * m_weights(source, dest) * m_rates(source, dest);
			sum_probas += m_probabilities(source, dest);
		}
		m_probabilities(source, source) = 1. - sum_probas;
    }

protected:
	// rows = sources of transfers, columns = destinations of transfers
	Matrix_D m_rates;
	Matrix_D m_weights;				// to ponder probabilities
	Matrix_D m_probabilities;
	Matrix_UI m_transfers;			// number of animals transfered from state X to state Y

	// for optimization (to list each nonzero elements in matrices)
	// to iterate over nonzero elements
	vector<unsigned int> v_sources;
	map<unsigned int, vector<unsigned int> > map_destinations;	// one element = (source, vector of destinations)

    // see multinomial method from GSL library for the algorithm
    void calculate_multinomial(const int source, unsigned int nb_animals){
    	if(nb_animals > 0){
    		// sum of probabilities = 1.
			double coeff_normalization = 1.0;
			for(unsigned int dest : map_destinations[source]){
				m_transfers(source, dest) = binomial(m_probabilities(source, dest) / coeff_normalization, nb_animals);
				coeff_normalization -= m_probabilities(source, dest);
				nb_animals -= m_transfers(source, dest);
			}
    	}
    }

public:
	Transfer(){
		m_rates.setZero();
        m_weights.setZero();
        m_transfers.setZero();
		m_probabilities.setZero();
	}

	virtual ~Transfer(){ }

    void add_new_transfer(const int source, const int destination, const double rate, const double weight=1.0){
    	if(source == destination) throw invalid_argument("source and destination must be different");
    	if( find(v_sources.begin(), v_sources.end(), source) == v_sources.end() ) v_sources.push_back(source);
    	map_destinations[source].push_back(destination);
    	m_weights(source, destination) = weight;
    	change_rate(source, destination, rate);
    }

    void change_rate(const int source, const int destination, const double rate){
    	m_rates(source, destination) = rate;
    	calculate_probabilities(source);
    }

    void change_weight(const int source, const int destination, const double weight){
    	m_weights(source, destination) = weight;
    	calculate_probabilities(source);
    }

    unsigned int get_transfer(const int source, const int destination){
    	return m_transfers(source, destination);
    }

    unsigned int get_sum_transfers_from(const int source){
    	return m_transfers.row(source).sum();
    }

    unsigned int get_sum_transfers_to(const int destination){
    	return m_transfers.col(destination).sum();
    }

    /************ for debug **************/
    Matrix_UI get_transfers_matrix() const{
        return m_transfers;
    }

    Matrix_D get_rates_matrix() const{
    	return m_rates;
    }

    Matrix_D get_probabilities_matrix() const{
    	return m_probabilities;
    }

    void print_rates_weights_probas(){
    	cout << "rate, weight, proba: " << endl;
    	for(unsigned int source : v_sources)
    		for(unsigned int dest : map_destinations[source])
    			cout << "[" << source << "->" << dest << "] " << m_rates(source, dest) << " " <<
    				m_weights(source, dest) << " " << m_probabilities(source, dest) << endl;
    	cout << "\n" << endl;
    }
};

}
