
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "../includes.h"
#include "HerdCalendar.h"

namespace model {

enum enum_fertility{
	empty_female, undetermined, pregnant_female
};

class Generate_Calving_Dates {
private:
    Period& repro_heifers;
    Period& repro_cows;
    Period& calving_period;

    boost::random::gamma_distribution<> dist_gamma;

	/*! Generates one calving date using a Gamma distribution.
     *  \return calving date (in steps) or empty date */
    int generate_calving_date(const int date_available, const double fertility){
        Parameters& iParams = Parameters::getInstance();
    	int calving_date = iParams.emptyDate;
    	// check if cow is fertile
        if(bernoulli(fertility)){
        	int date_start_pregnancy = date_available + static_cast<int>(dist_gamma(iParams.gen) / iParams.daysNbPerTimeStep);
        	calving_date = date_start_pregnancy + iParams.gestatingDuration;
            if(calving_date % iParams.weeksNbPerYear < calving_period.start % iParams.weeksNbPerYear ||
               calving_date % iParams.weeksNbPerYear > calving_period.end % iParams.weeksNbPerYear)
            	calving_date = iParams.emptyDate;
        }
        return calving_date;
	}

public:
    Generate_Calving_Dates(Herd_Calendar* calendar) : repro_heifers(calendar->repro_heifers),
    		repro_cows(calendar->repro_cows), calving_period(calendar->calving_period){
        Parameters& iParams = Parameters::getInstance();
        dist_gamma = boost::random::gamma_distribution<>{iParams.gammaA, iParams.gammaB};
	}

	virtual ~Generate_Calving_Dates(){ }

    int get_next_calving_date(const int calving_date){
        Parameters& iParams = Parameters::getInstance();
    	int date_first_cycle = calving_date + iParams.weeksNbWithoutBreedAfterCalving;
    	int date_available = (date_first_cycle < repro_cows.start) ? repro_cows.start : date_first_cycle;
        return generate_calving_date(date_available, iParams.cowsFertilityRate);
    }

    int get_calving_date_after_abortion(const int date_abort){
        Parameters& iParams = Parameters::getInstance();
        int date_first_cycle = date_abort + iParams.weeksNbWithoutBreedAfterAbort;
        return (repro_cows.start <= date_first_cycle && date_first_cycle <= repro_cows.end) ?
            generate_calving_date(date_first_cycle, iParams.cowsFertilityRate) : iParams.emptyDate;
    }

    /*! \warning If the female is introduced before the calving period, she will give birth to a calf during the
     *  introduction year. If not, she will give birth to a calf the next year after introduction. */
    int generate_new_calving_date(const int week_introduction, const bool isCow, const enum_fertility type){
        Parameters& iParams = Parameters::getInstance();
    	double fertility = 0.;
    	switch(type){
    		case empty_female:
    			fertility = 0.;
    			break;
    		case undetermined:
    		    fertility = (isCow) ? iParams.cowsFertilityRate : iParams.heifersFertilityRate;
    		    break;
    		case pregnant_female:
    		    fertility = 1.;
    		    break;
    	}
    	int date_available = (isCow) ? repro_cows.start : repro_heifers.start;
    	if(week_introduction <= calving_period.start) date_available -= iParams.weeksNbPerYear;
        return generate_calving_date(date_available, fertility);
    }
};

}
