
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "Transfer.h"

namespace model {

class Transfer_Comps : public Transfer {
private:
	Vector_UI& v_compartments;

public:
	Transfer_Comps(Vector_UI& v_compartments) : v_compartments(v_compartments){ }
	~Transfer_Comps(){ }

	void calculate_and_make_transfers(){
		m_transfers.setZero();
		for(unsigned int source : v_sources)
			if(v_compartments(source) > 0) calculate_multinomial(source, v_compartments(source));

		for(unsigned int source : v_sources){
			if(v_compartments(source) > 0){
				for(unsigned int destination : map_destinations[source]){
					v_compartments(source) -= m_transfers(source, destination);
					v_compartments(destination) += m_transfers(source, destination);
				}
			}
		}
	}
};

}
