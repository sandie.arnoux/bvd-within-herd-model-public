
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Parameters.h"

Parameters *Parameters::instance = NULL;

Parameters::Parameters() {
    daysNbPerWeek = 7;
    weeksNbPerYear = 52;
    daysNbPerTimeStep = 7;
    stabilizationYearsNb = 4;

    sexRatio = 0.5; // probality for a calf to be a male;
    weeksNbBetweenHeifCowsBreedStarts = 2;
    weeksNbWithoutBreedAfterCalving = 3;
    weeksNbWithoutBreedAfterAbort = 8;
    emptyDate = -1;
    maxFemalesNbPerBull = 20;
    gestatingDuration = 41;
    gammaA = 8.7;
    gammaB = 6.;

    weeksNbAfterIndoorStartToSellEmpty = 14; //100 days
    weeksNbAfterCalvingEndToPurchaseCalf = 13; //3 months

    probaOfPDeathAtBirth = 0.0666; // if Rb: 70% P and 5% death at birth, so "5% dead among 75%" => over 100% it's 6.66%
    probaOfPDeathPerYear = 0.5;
    mu_P = 1. - pow((1. - probaOfPDeathPerYear), (1. / 365.));
    beta_T = 0.03;
    beta_P = 0.5;
    beta = 0.1;
    rate_M_S = 1. / 150.;
    rate_T_R = 1. / 5.;
    probaAbortionIfRa = 0.8;
    probaAbortionIfRb = 0.2;
    probaPbirthIfRb = 0.75; // if Rb, 20% abortion, 75% P (70% P + 5% dead), 5% M or R, so "75% P among 80%" => over 100% it's 93.75%
    probaMbirthIfRb = 0.025;    //idem: "2.5% among 80%" => 3.125%
    probaRbirthIfRb = 0.025;    //idem
    lastGestatingWeekNumOfRa = 6;
    lastGestatingWeekNumOfRb = 21;
    lastGestatingWeekNumOfRc = gestatingDuration;

    /************** params changed by the user *************/
    yearsNb = 10;
    runsNb = 1000;
    areTheSeedsToBeRead = false;
    seedFileName = "seeds.txt";
    initPopFileName = "";
    endPopFileName = "";
    headcountFolder = "../results/headcount";
    isInitializationOfPastureWithRprop = false;
	pastureInitFileName = "";
    femalesRprop = 0;
    areHeadcountsToBeSavedInFiles = false;

    ageGroupToInfect = -1;
    ageGroupToProtect = -1;
    nbToMoveFromSToP = 0;
    propToMoveFromSToR = 0;

    renewalRate = 2; //young heifers for sale
    breedHeifNb = 22;
    breedCowsNb = 68;
    riskFactor = 1.0;

    weaningDate = "20151014";        //0
    heifBreedStartDate = "20150322"; //22
    breedEndDate = "20150719";       //39
    indoorStartDate = "20151123";    //5
    pastureStartDate = "20150401";   //24

    deadCalvesTotalProp = 0.08;
    deadCalvesAfterBirthProp = deadCalvesTotalProp * 0.75;
    deadCalvesAtBirthProp = deadCalvesTotalProp * 0.25;
    mu_Ca = 1. - pow((1. - deadCalvesAfterBirthProp), (1. / 190.));
    twinningRate = 0.023;
    heifersFertilityRate = 0.961;
    cowsFertilityRate = 0.939;

    malesSoldAtAutumnProp = 1.;
    malesSoldAtWinterProp = 0.;
    malesSoldAtSpringProp = 0.;
    femalesSoldAtAutumnProp = 0.;
    femalesSoldAtWinterProp = 0.;
    femalesSoldAtSpringProp = 1.;

    isContactYoungHeifBreedHeif = true;
    isContactYoungHeifCows = true;
    isContactBreedHeifCows = true;
    isContactYoungHeifHerdOutside = true;
    isContactBreedHeifHerdOutside = true;
    isContactCalvesCowsHerdOutside = true;

    Kext = 0.;
    KextStartYearNum = -1;

    vaccinationScenario = 0;
    vaccStartYearNum = -1;
    vaccEndYearNum = -1;
    fullImmunityWeeksNb = 52;
    partialImmunityWeeksNb = 26;
    vaccHeifCoverage = 1.;
    vaccCowsCoverage = 1.;
    vaccEfficiency = 1.;    //if 1, animals won't be infected
    vaccEfficiencyAgainstSymptoms = 1.; //if 1, animals can be infected but won't shed (don't give birth to P)

    testCullStartYearNum = -1;
    testCullEndYearNum = -1;
    birthToCullWeeksNb = 8;         // [2, 16]
    detectionTestSensitivity = 1.;  // [0.9, 1.]

}

Parameters& Parameters::getInstance(){
	if (instance == NULL)
		instance = new Parameters();
	return *instance;
}

void Parameters::kill(){
    if (instance != NULL)
        delete instance;
    instance = NULL;
}
