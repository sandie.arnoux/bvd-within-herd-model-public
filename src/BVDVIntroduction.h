
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "includes.h"

struct BVDV_Introduction{
	enum e_types_intro{ //for interface, P_Calves = 1, but here P_Calves = 0, so Communicate_With_GUI::set_params_insert_of_an_infected_animal call add_infected_animal() with type_animal_infected - 1 !
		P_Calves, P_Young_Heifers, P_Pregnant_Heifers, P_Pregnant_Cows,
		RP_Bred_Heifers, RP_Cows, P_Bull_With_heifers, P_Bull_With_Cows,
		P_Bull
	};

	// temporary values (real parameters are initialized after the stabilization period)
	double proba_purchase_T_pregnant = 0.;
	double proba_purchase_P_pregnant = 0.;
	double proba_purchase_Rb_pregnant = 0.;
	double proba_purchase_T_calves = 0.;
	double proba_purchase_P_calves = 0.;
	double proba_purchase_P_bulls = 0.;

private:
	map<int, int> map_infected_animals;
	map<int, int>::iterator current_infected_animal;

public:
	BVDV_Introduction() { current_infected_animal = map_infected_animals.end(); }
	~BVDV_Introduction(){ map_infected_animals.clear(); }

	void reset_introduction_of_infected_animals(){
		current_infected_animal = map_infected_animals.begin();
	}

	int get_week_introduction() const{
		if(current_infected_animal != map_infected_animals.end()){
			return current_infected_animal->first;
		}else{
			return -1;
		}
	}

	int get_type_of_introduction() const{
		return current_infected_animal->second;
	}

	void add_infected_animal(int week, int type){
		map_infected_animals.insert(pair<int,int>(week, type));
	}

	/*! \note MUST be called after each introduction of an infected animal */
	void move_to_next_infected_animal_to_introduce(){
		current_infected_animal++;
	}
};
