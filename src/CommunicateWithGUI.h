
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "Simulation.h"

namespace model {

class CommunicateWithGUI {
private:
    Simulation* simu;

public:
    CommunicateWithGUI();
    virtual ~CommunicateWithGUI();
    void reset();

    void create_a_new_simulation(const int nb_repetitions, const int nb_total_years, const string weaning,
            const string start_repro_heifers, const string end_repro, const string start_indoor,
            const string end_indoor, const int nb_heifers_for_breeding,
            const int nb_cows_for_breeding, const int nb_young_heifers_for_sale,
            const float risk_factor);

    void set_params_calves(double proba_death_Ca, double twin, double fertil_H, double fertil_C);

    void set_params_sales(const double proportion_autumn_males, const double proportion_winter_males,
            const double proportion_spring_males, const double proportion_autumn_females,
            const double proportion_winter_females, const double proportion_spring_females);

    void set_params_purchases(const double proba_T_calves, const double proba_P_calves,
            const double proba_T_cows, const double proba_P_cows, const double proba_Rb_cows,
            const double proba_P_bulls);

    void set_params_epidemio(double probaOfPDeathAtBirth, double mu_P, double beta_T, double beta_P, double beta, double rate_M_S, double rate_T_R, double probaAbortionIfRa, double probaAbortionIfRb, double probaPbirthIfRb, double probaMbirthIfRb, double probaRbirthIfRb);

    void set_params_contacts(const double Kext, const bool contact_young_heifers_bred_heifers,
            const bool contact_young_heifers_cows, const bool contact_bred_heifers_cows,
            const bool contact_ext_young_heifers, const bool contact_ext_bred_heifers,
            const bool contact_ext_calves_cows, const int year_start_contamination_via_ext_or_purchases);

    void setPastureInitializationParameters(const bool doInitPastureFromFile, const string pastureInitFileName, const int ageGroupToInfect, const int ageGroupToProtect, const int nbToMoveFromSToP, const float propToMoveFromSToR);

    void set_params_vaccination(const int year_start_vaccination, const int year_end_vaccination,
            const int scenario, const int full_immunity_duration, const int partial_immunity_duration,
            const double coverage_heifers, const double coverage_cows, const double efficiency,
            const double protection_against_symptoms);

    void set_params_test_and_cull(const int year_start_test_and_cull, const int year_end_test_and_cull,
            const int delay_birth_culling, const double test_sensitivity);

    void set_params_insert_of_an_infected_animal(const int type_animal_infected, const int introduction_week);

    void set_params_seed(const bool areTheSeedsToBeRead);

    void set_save_in_files(const bool areHeadcountsToBeSavedInFiles);

    void run();

    void activate_save_population_herd(string file_name){
        Parameters::getInstance().initPopFileName = file_name;
    }

    void activate_load_population_herd(string file_name){
        Parameters::getInstance().endPopFileName = file_name;
    }

    void reset_all_data(){
        Results::get_Instance()->reset_all_data();
    }

    string get_name_output(const int entity){
        return Results::get_Instance()->get_name_entity(entity);
    }

    vector< vector<double> > get_data(const int entity){
        vector< vector<double> > data = Results::get_Instance()->get_data_entity_year(entity)->get_data();
        return data;
    }

    vector<double> get_time_vector(const int entity){
        vector<double> v_time =  Results::get_Instance()->get_data_entity_week(entity)->get_time_vector();
        return v_time;
    }

    vector<double> get_mean_vector(const int entity){
        vector<double> v_mean =  Results::get_Instance()->get_data_entity_week(entity)->get_mean_vector();
        return v_mean;
    }

    vector<double> get_nb_infected_animals_purchased(){
        return Results::get_Instance()->get_nb_infected_animals_purchased();
    }

    void print_data(const int entity, string st=""){
        Results::get_Instance()->get_data_entity_year(entity)->print_data(st);
    }

    vector<vector<double>> getExploreData(const int outputNum){
        return Explore::getInstance().get(outputNum);
    }

    vector<vector<double>> getExploreDataByGroup(const int outputNum, const int groupNum){
        return Explore::getInstance().getByGroup(outputNum, groupNum);
    }

    vector<vector<double>> getRnbPerWeekPerRun(){
        return Explore::getInstance().getRnbPerWeekPerRun();
    }

    vector<vector<double>> getTotnbPerWeekPerRun(){
        return Explore::getInstance().getTotnbPerWeekPerRun();
    }
};

}


#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
using namespace boost::python;
using namespace model;

BOOST_PYTHON_MODULE(libBVDV_intra_beef_cattle)
{
class_<vector<double> >("CVector")
    .def(vector_indexing_suite< vector<double> >())
    ;

class_<vector<vector<double> > >("CDoubleVector")
    .def(vector_indexing_suite< vector< vector<double> > >())
    ;

class_<CommunicateWithGUI>("CommunicateWithGUI")
    .def("reset", &CommunicateWithGUI::reset)
    .def("create_a_new_simulation", &CommunicateWithGUI::create_a_new_simulation)
    .def("set_params_calves", &CommunicateWithGUI::set_params_calves)
    .def("set_params_sales", &CommunicateWithGUI::set_params_sales)
    .def("set_params_purchases", &CommunicateWithGUI::set_params_purchases)
    .def("set_params_epidemio", &CommunicateWithGUI::set_params_epidemio)
    .def("set_params_contacts", &CommunicateWithGUI::set_params_contacts)
    .def("setPastureInitializationParameters", &CommunicateWithGUI::setPastureInitializationParameters)
    .def("set_params_vaccination", &CommunicateWithGUI::set_params_vaccination)
    .def("set_params_test_and_cull", &CommunicateWithGUI::set_params_test_and_cull)
    .def("set_params_insert_of_an_infected_animal", &CommunicateWithGUI::set_params_insert_of_an_infected_animal)
    .def("set_params_seed", &CommunicateWithGUI::set_params_seed)
    .def("set_save_in_files", &CommunicateWithGUI::set_save_in_files)
    .def("run", &CommunicateWithGUI::run)
    .def("activate_save_population_herd", &CommunicateWithGUI::activate_save_population_herd)
    .def("activate_load_population_herd", &CommunicateWithGUI::activate_load_population_herd)
    .def("reset_all_data", &CommunicateWithGUI::reset_all_data)
    .def("get_name_output", &CommunicateWithGUI::get_name_output)
    .def("get_data", &CommunicateWithGUI::get_data)
    .def("get_time_vector", &CommunicateWithGUI::get_time_vector)
    .def("get_mean_vector", &CommunicateWithGUI::get_mean_vector)
    .def("get_nb_infected_animals_purchased", &CommunicateWithGUI::get_nb_infected_animals_purchased)
    .def("print_data", &CommunicateWithGUI::print_data)
    .def("getExploreData", &CommunicateWithGUI::getExploreData)
    .def("getExploreDataByGroup", &CommunicateWithGUI::getExploreDataByGroup)
    .def("getRnbPerWeekPerRun", &CommunicateWithGUI::getRnbPerWeekPerRun)
    .def("getTotnbPerWeekPerRun", &CommunicateWithGUI::getTotnbPerWeekPerRun)
    ;
}
