
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "CommunicateWithGUI.h"

namespace model {

CommunicateWithGUI::CommunicateWithGUI(){
	simu = NULL;
}

CommunicateWithGUI::~CommunicateWithGUI() {
	delete simu;
}

void CommunicateWithGUI::reset(){
	if( simu != NULL ){
		reset_all_data();
		delete simu;
	}
}

void CommunicateWithGUI::create_a_new_simulation(const int nb_repetitions, const int nb_total_years,
		const std::string weaning, const std::string start_repro_heifers, const std::string end_repro, const std::string start_indoor,
		const std::string end_indoor, const int nb_heifers_for_breeding, const int nb_cows_for_breeding,
		const int nb_young_heifers_for_sale, const float risk_factor){
    Parameters& iParams = Parameters::getInstance();
    iParams.yearsNb = nb_total_years;
    iParams.runsNb = nb_repetitions;

    iParams.renewalRate = nb_young_heifers_for_sale;
    iParams.breedHeifNb = nb_heifers_for_breeding;
    iParams.breedCowsNb = nb_cows_for_breeding;
    iParams.riskFactor = risk_factor;

    iParams.weaningDate = weaning;
    iParams.heifBreedStartDate = start_repro_heifers;
    iParams.breedEndDate = end_repro;
    iParams.indoorStartDate = start_indoor;
    iParams.pastureStartDate = end_indoor;
    
	simu = new Simulation();
}

void CommunicateWithGUI::set_params_calves(double proba_death_Ca, double twin, double fertil_H, double fertil_C){
    Parameters& iParams = Parameters::getInstance();
    iParams.deadCalvesTotalProp = proba_death_Ca;
    iParams.deadCalvesAfterBirthProp = proba_death_Ca * 0.75;
    iParams.deadCalvesAtBirthProp = proba_death_Ca * 0.25;
    iParams.twinningRate = twin;
    iParams.heifersFertilityRate = fertil_H;
    iParams.cowsFertilityRate = fertil_C;
    iParams.mu_Ca = 1. - std::pow((1. - iParams.deadCalvesAfterBirthProp), (1. / 190.));
}

void CommunicateWithGUI::set_params_sales(const double proportion_autumn_males, const double proportion_winter_males, 
        const double proportion_spring_males, const double proportion_autumn_females, 
        const double proportion_winter_females, const double proportion_spring_females){
    if(abs(proportion_autumn_males + proportion_winter_males + proportion_spring_males - 1.) > 0.00001)
        throw invalid_argument("sales of male grassers: sum of probabilities is not equal to 1");
    if(abs(proportion_autumn_females + proportion_winter_females + proportion_spring_females - 1.) > 0.00001)
        throw invalid_argument("sales of female grassers: sum of probabilities is not equal to 1");

    Parameters& iParams = Parameters::getInstance();
	iParams.malesSoldAtAutumnProp = proportion_autumn_males;
    iParams.malesSoldAtWinterProp = proportion_winter_males;
    iParams.malesSoldAtSpringProp = proportion_spring_males;
    iParams.femalesSoldAtAutumnProp = proportion_autumn_females;
    iParams.femalesSoldAtWinterProp = proportion_winter_females;
    iParams.femalesSoldAtSpringProp = proportion_spring_females;
}

void CommunicateWithGUI::set_params_purchases(const double proba_T_calves,
		const double proba_P_calves, const double proba_T_cows, const double proba_P_cows,
		const double proba_Rb_cows, const double proba_P_bulls){
	simu->get_BVDV_intro().proba_purchase_T_calves = proba_T_calves;
	simu->get_BVDV_intro().proba_purchase_P_calves = proba_P_calves;
	simu->get_BVDV_intro().proba_purchase_T_pregnant = proba_T_cows;
	simu->get_BVDV_intro().proba_purchase_P_pregnant = proba_P_cows;
	simu->get_BVDV_intro().proba_purchase_Rb_pregnant = proba_Rb_cows;
	simu->get_BVDV_intro().proba_purchase_P_bulls = proba_P_bulls;
}

void CommunicateWithGUI::set_params_epidemio(double probaOfPDeathAtBirth, double mu_P, double beta_T, double beta_P, double beta,
        double rate_M_S, double rate_T_R, double probaAbortionIfRa, double probaAbortionIfRb, double probaPbirthIfRb, double probaMbirthIfRb, double probaRbirthIfRb){
    Parameters& iParams = Parameters::getInstance();
    iParams.probaOfPDeathAtBirth = probaOfPDeathAtBirth;
    iParams.mu_P = mu_P;
    iParams.beta_T = beta_T;
    iParams.beta_P = beta_P;
    iParams.beta = beta;
    iParams.rate_M_S = rate_M_S;
    iParams.rate_T_R = rate_T_R;
    iParams.probaAbortionIfRa = probaAbortionIfRa;
    iParams.probaAbortionIfRb = probaAbortionIfRb;
    iParams.probaPbirthIfRb = probaPbirthIfRb;
    iParams.probaMbirthIfRb = probaMbirthIfRb;
    iParams.probaRbirthIfRb = probaRbirthIfRb;
}

void CommunicateWithGUI::set_params_contacts(const double Kext,
		const bool contact_young_heifers_bred_heifers, const bool contact_young_heifers_cows,
		const bool contact_bred_heifers_cows, const bool contact_ext_young_heifers,
		const bool contact_ext_bred_heifers, const bool contact_ext_calves_cows,
		const int year_start_contamination_via_ext_or_purchases){
    Parameters& iParams = Parameters::getInstance();
	iParams.Kext = Kext;
	iParams.KextStartYearNum = year_start_contamination_via_ext_or_purchases;
    iParams.isContactYoungHeifBreedHeif = contact_young_heifers_bred_heifers;
    iParams.isContactYoungHeifCows = contact_young_heifers_cows;
    iParams.isContactBreedHeifCows = contact_bred_heifers_cows;
    iParams.isContactYoungHeifHerdOutside = contact_ext_young_heifers;
    iParams.isContactBreedHeifHerdOutside = contact_ext_bred_heifers;
    iParams.isContactCalvesCowsHerdOutside = contact_ext_calves_cows;
}

void CommunicateWithGUI::setPastureInitializationParameters(const bool doInitPastureFromFile, const string pastureInitFileName, const int ageGroupToInfect, const int ageGroupToProtect, const int nbToMoveFromSToP, const float propToMoveFromSToR){
    Parameters& iParams = Parameters::getInstance();
	iParams.isInitializationOfPastureWithRprop = doInitPastureFromFile;
	iParams.pastureInitFileName = pastureInitFileName;
    iParams.ageGroupToInfect = ageGroupToInfect;
    iParams.ageGroupToProtect = ageGroupToProtect;
    iParams.nbToMoveFromSToP = nbToMoveFromSToP;
    iParams.propToMoveFromSToR = propToMoveFromSToR;
}

void CommunicateWithGUI::set_params_vaccination(const int year_start_vaccination, const int year_end_vaccination,
		const int scenario, const int full_immunity_duration, const int partial_immunity_duration,
		const double coverage_heifers, const double coverage_cows, const double efficiency,
		const double protection_against_symptoms){
    if(full_immunity_duration > 52)
        throw invalid_argument("duration of full immunity can't exceed 52 weeks");
    if(coverage_heifers < 0. || coverage_heifers > 1.0 || coverage_cows < 0. || coverage_cows > 1.0)
        throw invalid_argument("vaccination coverage must be in range [0., 1.]");
    if(efficiency < 0. || efficiency > 1.0)
        throw invalid_argument("value of efficiency of vaccine must be in range [0., 1.]");
    if(protection_against_symptoms < 0. || protection_against_symptoms > 1.0)
        throw invalid_argument("value of protection against symptoms must be in range [0., 1.]");

    Parameters& iParams = Parameters::getInstance();
    iParams.vaccinationScenario = scenario;
    iParams.vaccStartYearNum = year_start_vaccination;
    if (iParams.vaccinationScenario == 0)
        iParams.vaccStartYearNum = -1; //to avoid bug (heifers vaccinated even if vaccinationScenario = 0...)
    iParams.vaccEndYearNum = year_end_vaccination;
    iParams.fullImmunityWeeksNb = full_immunity_duration;
    iParams.partialImmunityWeeksNb = partial_immunity_duration;
    iParams.vaccHeifCoverage = coverage_heifers;
    iParams.vaccCowsCoverage = coverage_cows;
    iParams.vaccEfficiency = efficiency;
    iParams.vaccEfficiencyAgainstSymptoms = protection_against_symptoms;
}

void CommunicateWithGUI::set_params_test_and_cull(const int year_start_test_and_cull, const int year_end_test_and_cull,
		const int delay_birth_culling, const double test_sensitivity){
    if(test_sensitivity < 0. || test_sensitivity > 1.)
        throw invalid_argument("sensitivity of the test must be in range [0., 1.]");

    Parameters& iParams = Parameters::getInstance();
	iParams.testCullStartYearNum = year_start_test_and_cull;
    iParams.testCullEndYearNum = year_end_test_and_cull;
	iParams.birthToCullWeeksNb = delay_birth_culling;
    iParams.detectionTestSensitivity = test_sensitivity;
}

void CommunicateWithGUI::set_params_insert_of_an_infected_animal(const int type_animal_infected,
		const int introduction_week){
	simu->get_BVDV_intro().add_infected_animal(introduction_week, type_animal_infected-1);
}

void CommunicateWithGUI::set_params_seed(const bool areTheSeedsToBeRead){
    Parameters::getInstance().areTheSeedsToBeRead = areTheSeedsToBeRead;
}

void CommunicateWithGUI::set_save_in_files(const bool areHeadcountsToBeSavedInFiles){
    Parameters::getInstance().areHeadcountsToBeSavedInFiles = areHeadcountsToBeSavedInFiles;
}

void CommunicateWithGUI::run(){
	simu->repetitions();
}

}
