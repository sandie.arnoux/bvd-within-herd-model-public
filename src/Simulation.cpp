
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Simulation.h"

namespace model {

Simulation::Simulation(){
	herd = new Herd();
	Results::get_Instance()->prepare_data_entities();
    Explore::getInstance().createOutputsVector();
    Explore::getInstance().createOutputsByGroupVector();
}

Simulation::~Simulation() {
	delete herd;
	Results::kill();
    Explore::kill();
    Parameters::kill();
}

void Simulation::repetitions(){
    Parameters& iParams = Parameters::getInstance();
    vSeeds.resize(iParams.runsNb);
    if (iParams.areTheSeedsToBeRead){
        ifstream fSeedsIn(iParams.seedFileName.c_str());
        if (not fSeedsIn) throw runtime_error("ERROR: can't read "+ iParams.seedFileName +" file.");
        for (int i = 0; i < iParams.runsNb; ++i)
            fSeedsIn >> vSeeds[i];
        fSeedsIn.close();
    } else {
        for (int i = 0; i < iParams.runsNb; ++i)
            vSeeds[i] = 0;
    }

	ifstream fInitPop;
	if (iParams.initPopFileName != ""){
		fInitPop.open(iParams.initPopFileName.c_str());
		if (not fInitPop) throw runtime_error("ERROR: can't read "+ iParams.initPopFileName +" file.");
	}
    ofstream fEndPop;
	if (iParams.endPopFileName != ""){
		fEndPop.open(iParams.endPopFileName.c_str());
	    if (not fEndPop) throw runtime_error("ERROR: can't create "+ iParams.endPopFileName +" file.");
	}
    ifstream fInitPasture;
	if (iParams.isInitializationOfPastureWithRprop){
        fInitPasture.open(iParams.pastureInitFileName.c_str());
        if (not fInitPasture) throw runtime_error("ERROR: can't read "+ iParams.pastureInitFileName +" file.");
    }

    for (int runNum = 0; runNum < iParams.runsNb; runNum++){
        change_seed(vSeeds[runNum]);
        if (herd != NULL) delete herd;
        herd = new Herd();

    	bvdv.reset_introduction_of_infected_animals();

	    if (iParams.initPopFileName != ""){
    		herd->initialize_population_from_file(fInitPop);
    	} else {
    		herd->init_population();
    		stabilization_period();
    	}
	    if (iParams.isInitializationOfPastureWithRprop)
            fInitPasture >> iParams.femalesRprop;

    	one_repetition();

        iParams.femalesRprop = 0.;
	    if (iParams.endPopFileName != "")
			herd->print_herd(fEndPop, runNum);
        Explore::getInstance().incrementRunNum();
    }

	if (iParams.initPopFileName != "")
        fInitPop.close();
	if (iParams.endPopFileName != "")
        fEndPop.close();
	if (iParams.isInitializationOfPastureWithRprop)
        fInitPasture.close();

//    if (not iParams.areTheSeedsToBeRead){
//        ofstream fSeedsOut(iParams.seedFileName.c_str());
//        if (not fSeedsOut) throw runtime_error("ERROR: can't create "+ iParams.seedFileName +" file.");
//        for (int i = 0; i < iParams.runsNb; ++i)
//            fSeedsOut << vSeeds[i] << endl;
//        fSeedsOut.close();
//    }
}

void Simulation::stabilization_period(){
    Explore::getInstance().setIsStabilisationPeriod(true);
    Parameters& iParams = Parameters::getInstance();
	int year = -1;
	// loop stop just before a new year
	for (int week = 0; week < iParams.stabilizationYearsNb * iParams.weeksNbPerYear; week++){
		// happy new year!
		if (week % iParams.weeksNbPerYear == 0){
			year++;
			if (year > 0)
				herd->plus_one_year();
			herd->prepare_dates_to_sell_grassers(year);

			herd->save_and_reset_deaths();
			herd->save_and_reset_abortions();
			Results::get_Instance()->reset_temporary_data();
		}
		herd->manage_special_dates(week);
		herd->calvings(week);
		herd->sell_grassers(week);
		herd->purchase_calves(week);
		herd->make_transfers_between_health_states(week);
	}
    Explore::getInstance().setIsStabilisationPeriod(false);
}

void Simulation::one_repetition(){
    Parameters& iParams = Parameters::getInstance();
    Results::get_Instance()->reset_temporary_data();
    Explore::getInstance().resetYearNum();
	int year = -1;
	herd->shift_to_year_0();
    
	for (int week = 0; week <= iParams.yearsNb * iParams.weeksNbPerYear; week++){
        iParams.weekNum = week;
		start_the_clock();
		// happy new year!
		if (week % iParams.weeksNbPerYear == 0){
			year++;
			if (year > 0)
				herd->plus_one_year();
			herd->prepare_dates_to_sell_grassers(year);

			if (iParams.KextStartYearNum == year)
				herd->activate_Kext_and_purchase_of_infected_animals(bvdv);

			if (year == iParams.vaccStartYearNum) herd->activate_vaccination();
			if (year == iParams.vaccEndYearNum)   herd->desactivate_vaccination();

			herd->save_and_reset_deaths();
			herd->save_and_reset_abortions();
			Results::get_Instance()->add_data_year(Results::Persistence) =
					(herd->is_BVDV_present_in_the_herd()) ? 1.0 : 0.0;

			Results::get_Instance()->push_data_year(year);
		}

		// insert BVDV (forced introduction)
		if(bvdv.get_week_introduction() == week){
			herd->insert_infected_animal(week, bvdv.get_type_of_introduction());
			bvdv.move_to_next_infected_animal_to_introduce();
		}

		// special dates (weaning, start/end of breeding period, ...)
		herd->manage_special_dates(week);
		add_execution_time();

		// calvings
		herd->calvings(week);
		add_execution_time();

		// sales and purchases
		herd->sell_grassers(week);
		herd->purchase_calves(week);
		add_execution_time();

		// transmission force
		herd->calculate_transmission_force(week);
		add_execution_time();

		// transfers between health states
		herd->make_transfers_between_health_states(week);
		add_execution_time();

		// collect of weekly data
		herd->save_T_P_RP_and_pop_per_group(week);
        if (iParams.areHeadcountsToBeSavedInFiles)
            herd->saveHeadcountPerStatePerGroupInFile();
	}
}

}
