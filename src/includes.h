
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include <map>
#include <array>
#include <vector>
#include <algorithm>
#include <iterator>
#include <fstream>

#include "boost/random/discrete_distribution.hpp"
#include <boost/date_time/gregorian/gregorian.hpp>

#define EIGEN_DEFAULT_TO_ROW_MAJOR
#define EIGEN_INITIALIZE_MATRICES_BY_ZERO
#include <eigen3/Eigen/Core>

#include "random_distributions.h"
#include "Parameters.h"
#include "RESULTS/Explore.h"

using namespace std;
using namespace Eigen;
using namespace boost::gregorian;

typedef Matrix<unsigned int, 1, 12> Vector_UI;
typedef Matrix<unsigned int, 12, 12> Matrix_UI;
typedef Matrix<double, 1, 12> Vector_D;
typedef Matrix<double, 12, 12> Matrix_D;

#define SIZE_EIGEN_VECTOR 12
#define ALL 100000
