
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "DataEntitySaveAll.h"

namespace model {

DataEntitySaveAll::DataEntitySaveAll(string name, const int nb_timesteps, double timestep):
		DataEntity(name, nb_timesteps, timestep){
	vector<double> tmp_vector;
	for(int i=0; i<nb_timesteps; i++){
		v_data.push_back(tmp_vector);
		v_data.back().reserve(Parameters::getInstance().runsNb);
	}
}

DataEntitySaveAll::~DataEntitySaveAll(){
	reset();
	v_data.clear();
}

void DataEntitySaveAll::print_data(string st){
	pFileData.open("./" + st + "_data_" + name + ".txt");
    int time = 0;
    for(vector<double> vec : v_data){
        pFileData << time;
        for(double data : vec) pFileData << "; " << data ;
        pFileData << endl;
        time += timestep;
    }
    pFileData << "\n\n" << endl;

    pFileData.close();
}

}
