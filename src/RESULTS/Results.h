
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "../includes.h"
#include "DataEntitySaveAll.h"
#include "DataEntityUsingAccumulators.h"

namespace model {

/*! \note this class is implemented as a singleton
 *  \note all outputs are given by year and some by year and by week.\n
 *  by year: data for each year and each repetition are stored and then can be transfered to external code for analysis.\n
 *  by week: accumulators automatically calculate a mean value for each week, including all repetitions. */
class Results {
public: //declare enum before anything else (if not "error: ‘Last’ was not declared in this scope") even if public
	enum enum_type_infected_animal{
		Female_P, Female_T, Female_RP, Calve_P, Calve_T, Bull_P
	};

    enum enum_data_entities{
    	// (by year) : cumulated number of weeks of presence of animals in each group + total herd
    	// (by week) : number of animals present in each group + total herd (no TotalPop by week !!)
        Calves, Grassers, Young_Heifers, Bred_Heifers, Cows, Fattened_Cows, Fattened_Empty, Bulls, TotalPop,		// 0

        // (by year) : cumulated number of weeks of presence of S, T, R, P animals in each group + total herd
        S_Calves, S_Grassers, S_YoungHeif, S_Heif, S_Cows, S_Fat_Cows, S_Fat_empty, S_Bulls, S_tot,					// 9
        T_Calves, T_Grassers, T_YoungHeif, T_Heif, T_Cows, T_Fat_Cows, T_Fat_empty, T_Bulls, T_tot,  				// 18
        R_Calves, R_Grassers, R_YoungHeif, R_Heif, R_Cows, R_Fat_Cows, R_Fat_empty, R_Bulls, R_tot, 				// 27
        P_Calves, P_Grassers, P_YoungHeif, P_Heif, P_Cows, P_Fat_Cows, P_Fat_empty, P_Bulls, P_tot,         		// 36

        // (by year) : number of bred heifers and cows (fattened not included) in state V1 and V2 after vaccination (divided by pop)
        // (by week) : number of bred heifers and cows in state V1 and V2
        V1_Heif, V2_Heif, V1_Cows, V2_Cows,																			// 45

        // (by year) : cumulated number of weeks of presence of RP heifers and cows (fattened not included)
        // RP = carrying a PI foetus
        RP_Heif, RP_Cows, RP_tot,													        						// 49

        // (by year) : cumulated number of weeks of presence of bred heifers + cows (fattened not included)
        Breeding_tot,																								// 52

        // (by year) : if the virus is present in the herd at the end of the year = 1, else = 0
        // (by week) : if the virus is present in the herd = 1, else = 0
        // Virus is present if number of T animals + number of PI animals + number of RP females > 0
        Persistence,																								// 53

        // (by year) : number of new infections (= incidence) during the year for calves + total herd
        New_T_Calves, New_T,																						// 54
            //no transfer from S to T for bull...bull is only S or P.
            //New_T take into account calves (New_T_Calves is included in New_T)

        // (by year) : number of births of PI calves + number of P animals purchased
        New_P, 																										// 56

        // (by year) : number of non-PI calves dead
        Calve_Death,					        																	// 57

        // (by year) : number of PI calves dead (eliminated calves included)
        Calve_P_Death, 																								// 58

        // (by year) : number of PI animals dead (calves included)
        P_Death,																									// 59

        // (by year) : number of PI calves culled
        P_Calves_Culled,																							// 60

        // (by year) : number of bred heifers and cows who have aborted
        Abortion_Heif, Abortion_Cows, 																				// 61

        // (by year) : number of births (twins are counted for 2, calves dead at birth included)
        Birth, 			   																							// 63

        // (by year) : number of weaned calves
        Weaning,																									// 64

        // (by year) : number of pregnant bred heifers and cows (calculated at the beginning of the indoor period)
        Pregnant_Heif, Pregnant_Cows,																				// 65

        // (by year) : number of pregnant bred heifers and cows (calculated at the beginning of the indoor period)
        Empty_Heif, Empty_Cows,																						// 67

        // (by year) : number of male and female calves sold
        Sell_Male_Grassers, Sell_Female_Grassers, 																	// 69

        // (by year) : number of fat heifers sold because not selected for breeding
        Sell_Heifers, 																								// 71

        // (by year) : number of fattened females sold at weaning (= females not selected for breeding)
        Sell_Fattened_Females_At_Weaning,																			// 72

        // (by year) : number of replacement calves, pregnant females and bulls purchased
        Purchase_Calves, Purchase_Pregnant_Females,	Purchase_Bulls,	 												// 73

        // (by year) : cumulated number of weeks of presence of PI animals in each pasture
        // YH = young heifers, BH = bred heifers + 1 bull, CaCo = calves + cows (fattened included) + bulls
        P_pasture_YH, P_pasture_BH, P_pasture_CaCo,	  																// 76

        // (by year) : cumulated number of weeks of presence of animals in each pasture
        Pasture_YH, Pasture_BH, Pasture_CaCo,																		// 79
        Last // *************************************************************************************************** // 82
	};

private:
    DataEntitySaveAll* data_persistence_per_week;
    vector<DataEntityUsingAccumulators*> v_data_week;
    vector<DataEntitySaveAll*> v_data_year;
    static Results* results;

    array<double, Last> a_tmp_data;
    int nb_groups = 8;

    vector<string> v_data_entity_names = {
            "Calves", "Grassers", "Young_Heifers", "Bred_Heifers", "Cows", "Fattened_Cows", "Fattened_Empty", "Bulls", "TotalPop",
            "S_Calves", "S_Grassers", "S_YoungHeif", "S_Heif", "S_Cows", "S_Fat_Cows", "S_Fat_empty", "S_Bulls", "S_tot",
            "T_Calves", "T_Grassers", "T_YoungHeif", "T_Heif", "T_Cows", "T_Fat_Cows", "T_Fat_empty", "T_Bulls", "T_tot",
            "R_Calves", "R_Grassers", "R_YoungHeif", "R_Heif", "R_Cows", "R_Fat_Cows", "R_Fat_empty", "R_Bulls", "R_tot",
            "P_Calves", "P_Grassers", "P_YoungHeif", "P_Heif", "P_Cows", "P_Fat_Cows", "P_Fat_empty", "P_Bulls", "P_tot",
            "V1_Heif", "V2_Heif", "V1_Cows", "V2_Cows",
            "RP_Heif", "RP_Cows", "RP_tot",
            "Breeding_tot",
            "Persistence",
            "New_T_Calves", "New_T", "New_P",
            "Calve_Death", "Calve_P_Death", "P_Death",
            "P_Calves_Culled",
            "Abortion_Heif", "Abortion_Cows",
            "Birth", "Weaning", "Pregnant_Heif", "Pregnant_Cows", "Empty_Heif", "Empty_Cows",
            "Sell_Male_Grassers", "Sell_Female_Grassers", "Sell_Heifers", "Sell_Fattened_Females_At_Weaning",
            "Purchase_Calves", "Purchase_Pregnant_Females",	"Purchase_Bulls",
            "P_pasture_YH", "P_pasture_BH", "P_pasture_CaCo",
            "Pasture_YH", "Pasture_BH", "Pasture_CaCo"};

	Results(){}
	virtual ~Results(){
        reset();
    }

	void check_entity(const unsigned int entity){
		if(entity >= Last) throw invalid_argument("this entity does not exist");
	}

	void prepare_data(){
		for(int g=0; g<nb_groups; g++){
			a_tmp_data[TotalPop] += a_tmp_data[Calves + g];
			a_tmp_data[S_tot] += a_tmp_data[S_Calves + g];
			a_tmp_data[T_tot] += a_tmp_data[T_Calves + g];
			a_tmp_data[R_tot] += a_tmp_data[R_Calves + g];
			a_tmp_data[P_tot] += a_tmp_data[P_Calves + g];
		}
		a_tmp_data[Breeding_tot] = a_tmp_data[Bred_Heifers] + a_tmp_data[Cows];
		a_tmp_data[RP_tot] = a_tmp_data[RP_Heif] + a_tmp_data[RP_Cows];
	}

public:
	vector<double> v_nb_infected_animal_purchased = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	static Results* get_Instance(){
		if (results == NULL) results = new Results();
		return results;
	}

	static void kill(){
		if (results != NULL) delete results;
		results = NULL;
	}

	string get_name_entity(const int entity){
		check_entity(entity);
		return v_data_entity_names[entity];
	}

	void reset_temporary_data(){
		a_tmp_data.fill(0);
	}

	void reset(){
		a_tmp_data.fill(0);

		for(DataEntityUsingAccumulators* data_entity : v_data_week) delete data_entity;
		v_data_week.clear();

		for(DataEntitySaveAll* data_entity : v_data_year) delete data_entity;
		v_data_year.clear();

		delete data_persistence_per_week;
		for(double& data : v_nb_infected_animal_purchased) data = 0.0;
	}

	void prepare_data_entities(){
        Parameters& iParams = Parameters::getInstance();
		int nb_weeks = iParams.yearsNb * iParams.weeksNbPerYear;
		double deltaT = static_cast<double>(iParams.daysNbPerTimeStep);

        //WARNING: last data is empty: remove "+1" ? (nb_weeks+1, yearsNb+1, nb_weeks+1)
		for (int i = 0; i <= Persistence; i++) //a lot unused! Will be filled: [-1, 7], [45, 48], [53] (groups, V1/V2, persistence)
			v_data_week.push_back( new DataEntityUsingAccumulators(v_data_entity_names[i], nb_weeks+1, deltaT) );

		for (int i = 0; i < Last; i++)
			v_data_year.push_back( new DataEntitySaveAll(v_data_entity_names[i], iParams.yearsNb+1, 1.0) );

		data_persistence_per_week = new DataEntitySaveAll("Persistence_per_week", nb_weeks+1, deltaT);
	}

    double& add_data_year(const int entity){
    	check_entity(entity);
    	return a_tmp_data[entity];
    }

    void push_data_week(const int entity, const int week, const double data, const bool cumul_year=true){
    	check_entity(entity);
    	if(week < 0) throw invalid_argument("negative timestep");
    	v_data_week[entity]->push_data(week, data);
    	if(cumul_year) a_tmp_data[entity] += data;
    	if(entity == Persistence) data_persistence_per_week->push_data(week, data);
    }

    void push_data_year(const int year){
        if(year < 0) throw invalid_argument("negative timestep");
        prepare_data();
		for(int entity=0; entity<Last; entity++)
			v_data_year[entity]->push_data(year, a_tmp_data[entity]);
		reset_temporary_data();
    }

    void reset_all_data(){
    	for(DataEntityUsingAccumulators* data_entity : v_data_week) data_entity->reset();
    	for(DataEntitySaveAll* data_entity : v_data_year) data_entity->reset();

    	for(double& data : v_nb_infected_animal_purchased) data = 0.0;
    }

    DataEntityUsingAccumulators* get_data_entity_week(const int entity){
    	check_entity(entity);
    	return v_data_week[entity];
    }

    DataEntitySaveAll* get_data_entity_year(const int entity){
    	if(entity == -1){
    		return data_persistence_per_week;
    	}else{
    		check_entity(entity);
    		return v_data_year[entity];
    	}
	}

    vector<double> get_nb_infected_animals_purchased(){
    	return v_nb_infected_animal_purchased;
    }
};

}
