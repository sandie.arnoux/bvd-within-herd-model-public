
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "DataEntity.h"

namespace model {

class DataEntitySaveAll: public DataEntity {
private:
	// rows = timestep (year or week), columns = repetitions
	vector< vector<double> > v_data;

public:
	DataEntitySaveAll(string name, const int nb_timesteps, double timestep);
	virtual ~DataEntitySaveAll();

	void reset(){
		for(vector<double>& vec : v_data) vec.clear();
	}

	void push_data(const int timestep, const double data){
		v_data[timestep].push_back(data);
	}

	vector< vector<double> > get_data(){
		return v_data;
	}

	void print_data(string st);
};

}
