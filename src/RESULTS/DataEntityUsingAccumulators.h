
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "DataEntity.h"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/mean.hpp>

using namespace boost::accumulators;

typedef accumulator_set<double, stats<tag::count, tag::mean> > accu;

namespace model {

class DataEntityUsingAccumulators: public DataEntity {
private:
	vector<accu> v_accumulator;

	// outputs
	vector<double> v_time;
	vector<double> v_mean;

public:
	DataEntityUsingAccumulators(string name, const int nb_timesteps, double timestep);
	virtual ~DataEntityUsingAccumulators();

	void extract();

	void push_data(const int timestep, const double data){
		v_accumulator[timestep](data);
	}

	void reset(){
		for(double& mean : v_mean) mean = 0.0;
	}

	vector<double> get_time_vector(){
        return v_time;
    }

	vector<double> get_mean_vector(){
		extract();
		return v_mean;
    }

	void print(string directory);
};

}
