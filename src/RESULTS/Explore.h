
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include <cstddef>
#include <vector>
#include <iostream>

#include "../includes.h"

namespace model{

class Explore {
protected:
    static Explore* s_instance;
    Explore();

    bool _isStabilisationPeriod;
    bool _isPasturePeriod;
    int _runNum;
    int _yearNum;
    int _presWeekNbOfP;
    int _presWeekNbOfTot;
    std::vector<std::vector<std::vector<double>>> _vvvOutputsByYearByRun;
    std::vector<std::vector<std::vector<std::vector<double>>>> _vvvvOutputsByGroupByRunByYear;
    std::vector<std::vector<double>> _vvRnbPerWeekPerRun;
    std::vector<std::vector<double>> _vvTotnbPerWeekPerRun;

public:
    static Explore& getInstance();
    static void kill();
    enum OutputNames{ //Females == BH, C, FC, FCE (gestating)
        eNewT, eNewP, eIndivP, eFemalesR, eFemalesTot, eTot, ePresWeekNbOfP, ePresWeekNbOfTot, eNewTAtPasture, eNewRPAtPasture, eRTot, eV1, eV2, eSTotAtPastureStart, eNewS, ePresWeekNbOfS, eNewSAtPasture, ePresWeekNbOfPAtPasture, ePresWeekNbOfTotAtPasture, eLast
    };
    //NewS == births S + M to S + purchases S (but don't take into account bulls, because they can't be infected, so they would have distorted mean S duration)
    enum GroupNames{
        eCalves, eGrassers, eYoungHeifs, eBredHeifs, eCows, eFattened, eLastGroup
    };
    enum OutputsForGroup{
        eBG_Tot, eBG_RTot, eBG_NewT, eBG_NewTAtPasture, eBG_STotAtPastureStart, eBG_NewS, eBG_NewSAtPasture, eBG_PTotAtPastureStart, eLastOutputForGroup
    };
    void setIsStabilisationPeriod(bool isStabilisationPeriod);
    bool getIsStabilisationPeriod();
    void setIsPasturePeriod(bool isPasturePeriod);
    void createOutputsVector();
    void createOutputsByGroupVector();
    void incrementRunNum();
    void incrementYearNum();
    void resetYearNum();
    void add(int outputNum, int data);
    void addIfPasture(int outputNum, int data);
    void addByGroup(int outputNum, int groupNum, int data);
    void addByGroupIfPasture(int outputNum, int groupNum, int data);
    void print();
    std::vector<std::vector<double>> get(int outputNum);
    std::vector<std::vector<double>> getByGroup(int outputNum, int groupNum);
    void fillRnbPerWeek(int weekNum, int Rnb);
    void fillTotnbPerWeek(int weekNum, int totnb);
    std::vector<std::vector<double>> getRnbPerWeekPerRun();
    std::vector<std::vector<double>> getTotnbPerWeekPerRun();
};

}
