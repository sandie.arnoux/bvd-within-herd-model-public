
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "DataEntityUsingAccumulators.h"

namespace model {

DataEntityUsingAccumulators::DataEntityUsingAccumulators(string name, const int nb_timesteps, double timestep):
		DataEntity(name, nb_timesteps, timestep){
	for(int step = 0; step < nb_timesteps; step++){
		v_accumulator.push_back( accu() );
		v_time.push_back(step * timestep);
		v_mean.push_back(0.0);
	}

	v_accumulator.shrink_to_fit();
	v_time.shrink_to_fit();
	v_mean.shrink_to_fit();
}

DataEntityUsingAccumulators::~DataEntityUsingAccumulators(){
	v_time.clear();
	v_mean.clear();
	v_accumulator.clear();
}

void DataEntityUsingAccumulators::extract(){
	for(int step = 0; step < nb_timesteps; step++)
		v_mean[step] = mean( v_accumulator[step] );
}

void DataEntityUsingAccumulators::print(string directory){
    string fileName = directory + name + ".txt";
    ofstream file;
    file.open(fileName);

    file << "temps" << ";" << "moyenne" << ";" << "variance" << endl;

    for(int i = 0; i < nb_timesteps; i++)
        file << v_time[i] << ";" << v_mean[i] << endl;

    file.close();
}

}
