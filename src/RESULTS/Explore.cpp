
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Explore.h"

namespace model{

Explore* Explore::s_instance = NULL;

Explore::Explore(){
    _isStabilisationPeriod = false;
    _isPasturePeriod = false;
    _runNum = 0;
    _yearNum = 0;
    _presWeekNbOfP = 0;
    _presWeekNbOfTot = 0;
}

Explore& Explore::getInstance(){
    if (s_instance == NULL)
        s_instance = new Explore();
    return *s_instance;
}

void Explore::kill(){
    if (s_instance != NULL) delete s_instance;
    s_instance = NULL;
}

void Explore::setIsStabilisationPeriod(bool isStabilisationPeriod){
    _isStabilisationPeriod = isStabilisationPeriod;
}

bool Explore::getIsStabilisationPeriod(){
    return _isStabilisationPeriod;
}

void Explore::setIsPasturePeriod(bool isPasturePeriod){
    _isPasturePeriod = isPasturePeriod;
}

void Explore::createOutputsVector(){
    Parameters& iParams = Parameters::getInstance();
    _vvvOutputsByYearByRun.reserve(eLast);
    for (auto i = 0; i < eLast; ++i){
        std::vector<std::vector<double>> vvTemp(iParams.runsNb);
        for (auto j = 0; j < iParams.runsNb; ++j){
            //yearsNb+1 because for Explore, year start at pasture, but from t0 to first pasture, there are records, so there is one more value than years number !
            std::vector<double> vTemp(iParams.yearsNb + 1);
            for (auto k = 0; k < iParams.yearsNb + 1; ++k)
                vTemp[k] = 0;
            vvTemp[j] = vTemp;
        }
        _vvvOutputsByYearByRun.push_back(vvTemp);
    }

    int weeksNb = iParams.yearsNb * iParams.weeksNbPerYear +1; //weeks+1 because Alix chose it like this ("...for week <= totalWeeksNb..." in Simulation.cpp)
    _vvRnbPerWeekPerRun.reserve(iParams.runsNb);
    _vvTotnbPerWeekPerRun.reserve(iParams.runsNb);
    for (auto i = 0; i < iParams.runsNb; ++i){
        std::vector<double> vTemp(weeksNb);
        for (auto j = 0; j < weeksNb; ++j)
            vTemp[j] = 0;
        _vvRnbPerWeekPerRun.push_back(vTemp);
        _vvTotnbPerWeekPerRun.push_back(vTemp);
    }
}

void Explore::print(){
    std::cout << _yearNum << std::endl;
    for (auto i = 0; i < eLast; ++i){
        std::cout << "output " << i << std::endl;
        for (auto j = 0; j < _vvvOutputsByYearByRun[i].size(); ++j){
            for (auto k = 0; k < _vvvOutputsByYearByRun[i][j].size(); ++k)
                std::cout << _vvvOutputsByYearByRun[i][j][k] << " ";
            std::cout << std::endl;
        }
        std::cout << "-----------" << std::endl;
    }
    std::cout << "***" << std::endl << std::endl << std::endl;
}

void Explore::createOutputsByGroupVector(){
    Parameters& iParams = Parameters::getInstance();
    _vvvvOutputsByGroupByRunByYear.reserve(eLastOutputForGroup);
    for (auto i = 0; i < eLastOutputForGroup; ++i){
        std::vector<std::vector<std::vector<double>>> vvvTemp(eLastGroup);
        for (auto l = 0; l < eLastGroup; ++l){
            std::vector<std::vector<double>> vvTemp(iParams.runsNb);
            for (auto j = 0; j < iParams.runsNb; ++j){
                //yearsNb+1 because for Explore, year start at pasture, but from t0 to first pasture, there are records, so there is one more value than years number !
                std::vector<double> vTemp(iParams.yearsNb + 1);
                for (auto k = 0; k < iParams.yearsNb + 1; ++k){
                    vTemp[k] = 0;
                }
                vvTemp[j] = vTemp;
            }
            vvvTemp[l] = vvTemp;
        }
        _vvvvOutputsByGroupByRunByYear.push_back(vvvTemp);
    }
}

void Explore::incrementRunNum(){
    ++_runNum;
}

void Explore::incrementYearNum(){
    ++_yearNum;
}

void Explore::resetYearNum(){
    _yearNum = 0;
}

void Explore::add(int outputNum, int data){
    if (not _isStabilisationPeriod){
        _vvvOutputsByYearByRun[outputNum][_runNum][_yearNum] += data;
    }
}

void Explore::addByGroup(int outputNum, int groupNum, int data){
    if (not _isStabilisationPeriod){
        _vvvvOutputsByGroupByRunByYear[outputNum][groupNum][_runNum][_yearNum] += data;
    }
}

void Explore::addIfPasture(int outputNum, int data){
    if (not _isStabilisationPeriod and _isPasturePeriod){
        _vvvOutputsByYearByRun[outputNum][_runNum][_yearNum] += data;
    }
}

void Explore::addByGroupIfPasture(int outputNum, int groupNum, int data){
    if (not _isStabilisationPeriod and _isPasturePeriod){
        _vvvvOutputsByGroupByRunByYear[outputNum][groupNum][_runNum][_yearNum] += data;
    }
}

std::vector<std::vector<double>> Explore::get(int outputNum){
    return _vvvOutputsByYearByRun[outputNum];
}

std::vector<std::vector<double>> Explore::getByGroup(int outputNum, int groupNum){
    return _vvvvOutputsByGroupByRunByYear[outputNum][groupNum];
}

void Explore::fillRnbPerWeek(int weekNum, int Rnb){
    _vvRnbPerWeekPerRun[_runNum][weekNum] = Rnb;
}

void Explore::fillTotnbPerWeek(int weekNum, int totnb){
    _vvTotnbPerWeekPerRun[_runNum][weekNum] = totnb;
}

std::vector<std::vector<double>> Explore::getRnbPerWeekPerRun(){
    return _vvRnbPerWeekPerRun;
}

std::vector<std::vector<double>> Explore::getTotnbPerWeekPerRun(){
    return _vvTotnbPerWeekPerRun;
}

}
