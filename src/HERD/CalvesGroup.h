
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractNonGestatingGroup.h"

namespace model {

class Calves_Group: public model::Abstract_Non_Gestating_Group {
public:
    //calves M become S when they are not calves anymore (calves M -> Grassers S or YoungHeifers S)
	enum e{
		M_ = P_+1, D_Ca
	};

private:
	unsigned int objective_weaned_calves;
	int nb_calvings_to_come = 0;

public:
	Calves_Group(Herd_Calendar* calendar, unsigned int objective_weaned_calves) :
		Abstract_Non_Gestating_Group(calendar), objective_weaned_calves(objective_weaned_calves){
		nb_health_states = M_;
		init_transfers();
        set_proba_purchases(0, 0);
	}

	void init_transfers(){
        Parameters& iParams = Parameters::getInstance();
		Abstract_Non_Gestating_Group::init_transfers();
		transfMan->add_new_transfer(M_, S_, iParams.rate_M_S);
		transfMan->add_new_transfer(S_, D_Ca, iParams.mu_Ca);
		transfMan->add_new_transfer(T_, D_Ca, iParams.mu_Ca);
		transfMan->add_new_transfer(R_, D_Ca, iParams.mu_Ca);
		transfMan->add_new_transfer(M_, D_Ca, iParams.mu_Ca);
	}

	void make_transfers_between_health_states(const int week){
		Abstract_Non_Gestating_Group::make_transfers_between_health_states(week);
        int nbT = transfMan->get_transfer(S_, T_);
		Results::get_Instance()->add_data_year(Results::New_T_Calves) += nbT;
        int nbS = transfMan->get_transfer(M_, S_);
        Explore::getInstance().add(Explore::eNewS, nbS);
        Explore::getInstance().addIfPasture(Explore::eNewSAtPasture, nbS);
        Explore::getInstance().addByGroup(Explore::eBG_NewS, Explore::eCalves, nbS);
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewSAtPasture, Explore::eCalves, nbS);
        Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eCalves, nbT);
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eCalves, nbT);
	}

	void add_animals(int state, unsigned int nb_new_animals, int week=-100){
		v_compartments(state) += nb_new_animals;
	}

    //added by Alix to avoid +1 calf with BVD intro
    void replace_S_calve_by_P_calve(int week=-100){
        v_compartments(S_)--;
        add_animals(P_, 1, week);
    }

	unsigned int get_nb_animals_in_health_state(const char state){
		if(state == 'M'){
			return v_compartments(M_);
		}else{
			return Abstract_Non_Gestating_Group::get_nb_animals_in_health_state(state);
		}
	}

	unsigned int get_and_reset_nb_natural_deaths(){
		unsigned int nb_deaths = v_compartments(D_Ca);
		v_compartments(D_Ca) = 0;
		return nb_deaths;
	}

	void set_number_of_calvings_to_come(const unsigned int nb_calvings){
		nb_calvings_to_come = nb_calvings;
	}

	void decrease_nb_calvings_to_come(unsigned int nb_calvings){
		nb_calvings_to_come -= nb_calvings;
	}

	void set_proba_purchases(const double proba_T, const double proba_P){
		if( proba_T + proba_P > 1.0)
			throw invalid_argument("sum of probabilities is greater than 1");
		reset_proba_purchase();
		add_proba_purchase(S_, 1 - proba_T - proba_P);
		add_proba_purchase(T_, proba_T);
		add_proba_purchase(P_, proba_P);
	}

	void purchase(const int week){
		int nb_P_calves_purchased = 0;
		// eliminated calves from test and cull are not taken into account
	    int nb_new_deaths = transfMan->get_transfer(P_, D_) + transfMan->get_sum_transfers_to(D_Ca);

	    if(nb_new_deaths > 0 && nb_calvings_to_come + get_nb_animals_in_this_group() < objective_weaned_calves){
	        nb_P_calves_purchased = purchase_animals(nb_new_deaths);
	        Results::get_Instance()->add_data_year(Results::Purchase_Calves) += nb_new_deaths;
	    }
	}

	void transfer_to_group(Abstract_Non_Gestating_Group* group, const unsigned int nb_calves_for_transfer=ALL){
		vector<unsigned int> v_states = {S_, T_, R_, P_, M_};
		Vector_UI v_selection;
		if(nb_calves_for_transfer == ALL){
			v_selection = select_and_remove_all(v_states);
		}else{
			v_selection = random_select_and_remove(v_states, nb_calves_for_transfer);
		}

		// there is no compartment M in Grassers and Young Heifers
		v_selection(S_) += v_selection(M_);
        Explore::getInstance().add(Explore::eNewS, v_selection(M_));
        Explore::getInstance().addIfPasture(Explore::eNewSAtPasture, v_selection(M_));
        Explore::getInstance().addByGroup(Explore::eBG_NewS, Explore::eCalves, v_selection(M_));
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewSAtPasture, Explore::eCalves, v_selection(M_));
		v_selection(M_) = 0;
		group->add_animals(v_selection);
	}

};

}
