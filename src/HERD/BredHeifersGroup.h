
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractGestatingGroup.h"
#include "CowsGroup.h"
#include "FatteningGroup.h"

namespace model {

class Bred_Heifers_Group: public model::Abstract_Gestating_Group {
public:
	Bred_Heifers_Group(Herd_Calendar* calendar, Calves_Group* calves_group) :
		Abstract_Gestating_Group(calendar, calves_group, true){
        coverage_vaccination = Parameters::getInstance().vaccHeifCoverage;
    }

	virtual ~Bred_Heifers_Group(){ }

	void add_bred_heifers_and_generate_dates(const int week, const int list, const unsigned int nb_new_bred_heifers, const enum_fertility type){
		add_females_and_generate_dates(week, list, nb_new_bred_heifers, false, type);
	}

	void transfer_to_cows(Cows_Group* cows_group, const unsigned int nb_heifers_for_transfer=ALL){
		array_lists selection;
		if(nb_heifers_for_transfer == ALL){
			selection = select_and_remove_all();
		}else{
			selection = random_select_and_remove(nb_heifers_for_transfer);
		}

		cows_group->add_females_with_dates(selection);
	}

	void transfer_empty_females_to_fattened_females(Fattening_Group* fattened_females_group){
		transfer_empty_females_to_group(fattened_females_group);
	}

	void make_transfers_between_health_states(const int week){
		Abstract_Gestating_Group::make_transfers_between_health_states(week);
        int nbT = transfMan->get_sum_transfers_to(TR_);
        Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eBredHeifs, nbT);
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eBredHeifs, nbT);
    }
};

}
