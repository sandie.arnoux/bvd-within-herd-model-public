
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "CalvesGroup.h"
#include "GrassersGroup.h"
#include "YoungHeifersGroup.h"
#include "BredHeifersGroup.h"
#include "CowsGroup.h"
#include "FatteningGroup.h"
#include "Bulls.h"

namespace model {

struct Pasture{
private:
	vector<Abstract_Group*>		v_groups;

public:
	string name;
	vector<Bull*> v_bulls;
	unsigned int nb_animals = 0;
	unsigned int nb_T_animals = 0;
	unsigned int nb_P_animals = 0;

	Pasture(string name, vector<Abstract_Group*> v_groups) : name(name), v_groups(v_groups) { }
	~Pasture() { }

	void assign_one_bull(Bull* bull){
        v_bulls.push_back(bull);
    }

	void detach_all_bulls(){
        v_bulls.clear();
    }

	void replace_one_bull_by_a_P_bull() {
		if(v_bulls.size() == 0) throw runtime_error("there is no bull in pasture at that time");
		v_bulls[0]->replace_by_a_P_bull();
	}

	void calculate_total_T_and_P_animals(){
		nb_animals = 0;
		nb_T_animals = 0;
		nb_P_animals = 0;
		for(Abstract_Group* group : v_groups){
			nb_animals += group->get_nb_animals_in_this_group();
			nb_T_animals += group->get_nb_animals_in_health_state('T');
			nb_P_animals += group->get_nb_animals_in_health_state('P');
		}
		nb_animals += v_bulls.size();
		for(Bull* bull : v_bulls) if( bull->is_P() ) nb_P_animals++;
	}

	void set_transmission_force(const double transmission_force){
		for(Abstract_Group* group : v_groups) group->set_transmission_force(transmission_force);
	}
};

}
