
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Herd.h"

namespace model {

Herd::Herd(){
    Parameters& iParams = Parameters::getInstance();
    calendar = new Herd_Calendar();
    //calendar->print_dates();

    nb_heifers_for_renewal = iParams.breedHeifNb + iParams.renewalRate;
    nb_bulls_with_heifers = iParams.breedHeifNb / iParams.maxFemalesNbPerBull;
    if( iParams.breedHeifNb % iParams.maxFemalesNbPerBull > 0 ) nb_bulls_with_heifers++;
    nb_bulls_with_cows = iParams.breedCowsNb / iParams.maxFemalesNbPerBull;
    if( iParams.breedCowsNb % iParams.maxFemalesNbPerBull > 0 ) nb_bulls_with_cows++;
    nb_bulls = nb_bulls_with_heifers + nb_bulls_with_cows;

    // expected_nb_of_calvings
    double number = iParams.breedCowsNb * iParams.cowsFertilityRate + iParams.breedHeifNb * iParams.heifersFertilityRate;
    // objective number of pregnant females
    // ?? objective_pregnant_females = static_cast<unsigned int>( number ) * risk_factor; ??
    objective_pregnant_females = static_cast<unsigned int>( number );
    // expected_nb_of_births
    number *= (1. + iParams.twinningRate);
    // expected_nb_of_weaned_calves
    number *= (1. - iParams.deadCalvesTotalProp);
    // objective_weaned_calves
    number *= iParams.riskFactor;
    // objective number of weaned calves
    objective_weaned_calves = static_cast<int>(number);

    calves = new Calves_Group(calendar, objective_weaned_calves);
    grassers = new Grassers_Group(calendar);
    young_heifers_one_year = new Young_Heifers_Group(calendar);
    young_heifers_two_years = new Young_Heifers_Group(calendar);
    bred_heifers = new Bred_Heifers_Group(calendar, calves);
    cows = new Cows_Group(calendar, calves);
    fattened_cows = new Fattening_Group(calendar, calves);
    fattened_empty = new Fattening_Group(calendar, NULL);
    bulls = new Bulls_Group(calendar, nb_bulls);

    v_groups = {calves, grassers, young_heifers_one_year, young_heifers_two_years, bred_heifers, cows, fattened_cows, fattened_empty, bulls};
    v_gestating_groups = {bred_heifers, cows, fattened_cows, fattened_empty};

    v_pastures.push_back( Pasture("grassers", {grassers}) );
    v_pastures.push_back( Pasture("young heif", {young_heifers_one_year, young_heifers_two_years}) );
    v_pastures.push_back( Pasture("bred heif", {bred_heifers}) );
    v_pastures.push_back( Pasture("cows-calves", {calves, cows, fattened_cows}) );

    v_delta_Kext.assign(v_pastures.size(), false);
    v_delta_pastures.assign(v_pastures.size(), v_delta_Kext);
    if (iParams.isContactYoungHeifHerdOutside) set_delta_Kext(Pasture_Young_Heifers);
    if (iParams.isContactBreedHeifHerdOutside) set_delta_Kext(Pasture_Bred_Heifers);
    if (iParams.isContactCalvesCowsHerdOutside) set_delta_Kext(Pasture_Calves_Cows);
    if (iParams.isContactYoungHeifBreedHeif) set_delta_pastures(Pasture_Young_Heifers, Pasture_Bred_Heifers);
    if (iParams.isContactYoungHeifCows) set_delta_pastures(Pasture_Young_Heifers, Pasture_Calves_Cows);
    if (iParams.isContactBreedHeifCows) set_delta_pastures(Pasture_Bred_Heifers, Pasture_Calves_Cows);
}


Herd::~Herd(){
	delete calendar;
	delete calves;
	delete grassers;
	delete young_heifers_one_year;
	delete young_heifers_two_years;
	delete bred_heifers;
	delete cows;
	delete fattened_cows;
	delete fattened_empty;
	delete bulls;
}

void Herd::reset(){
	calendar->shift_to_year_0();

	for(Abstract_Group* group : v_groups){
		group->empty_this_group();
		group->set_transmission_force(0.);
		group->get_and_reset_nb_deaths();
	}

	bulls->reset();

	is_vaccination = false;
	is_BVDV_present = false;
	desactivate_Kext_and_purchase_of_infected_animals();
}

void Herd::print_herd(ofstream& file, const int repetition){
	file << "repetition number " << repetition << endl;

	file << calves->get_vector_population() << endl;
	file << grassers->get_vector_population() << endl;
	file << young_heifers_one_year->get_vector_population() << endl;
	file << young_heifers_two_years->get_vector_population() << endl;

	file << bred_heifers->get_vector_empty_females() << endl;
	file << bred_heifers->get_vector_pregnant_females() << endl;
	file << cows->get_vector_empty_females() << endl;
	file << cows->get_vector_pregnant_females() << endl;
	file << fattened_cows->get_vector_empty_females() << endl;
	file << fattened_cows->get_vector_pregnant_females() << endl;
	file << fattened_empty->get_vector_empty_females() << endl;

	file << bulls->get_nb_animals_in_health_state('S') << " " << bulls->get_nb_animals_in_health_state('P') << endl;

	file << endl;
	cout << "population herd have been saved in a file" << endl;
}

void Herd::initialize_population_from_file(ifstream& file){
	Vector_UI vector;

	string line;
	getline(file, line);

	for(int i=0; i<SIZE_EIGEN_VECTOR; i++) file >> vector(i);
	calves->init_population(vector);

	vector.setZero();
	for(int i=0; i<SIZE_EIGEN_VECTOR; i++) file >> vector(i);
	grassers->init_population(vector);

	vector.setZero();
	for(int i=0; i<SIZE_EIGEN_VECTOR; i++) file >> vector(i);
	young_heifers_one_year->init_population(vector);

	vector.setZero();
	for(int i=0; i<SIZE_EIGEN_VECTOR; i++) file >> vector(i);
	young_heifers_two_years->init_population(vector);

	unsigned int nb_females;
	for(int l=0; l<SIZE_EIGEN_VECTOR; l++){
		file >> nb_females;
		if(l<Abstract_Gestating_Group::nb_lists)
			bred_heifers->add_bred_heifers_and_generate_dates(0, l, nb_females, empty_female);
	}

	for(int l=0; l<SIZE_EIGEN_VECTOR; l++){
		file >> nb_females;
		if(l<Abstract_Gestating_Group::nb_lists)
			bred_heifers->add_bred_heifers_and_generate_dates(0, l, nb_females, pregnant_female);
	}

	for(int l=0; l<SIZE_EIGEN_VECTOR; l++){
		file >> nb_females;
		if(l<Abstract_Gestating_Group::nb_lists)
			cows->add_cows_and_generate_dates(0, l, nb_females, empty_female);
	}

	for(int l=0; l<SIZE_EIGEN_VECTOR; l++){
		file >> nb_females;
		if(l<Abstract_Gestating_Group::nb_lists)
			cows->add_cows_and_generate_dates(0, l, nb_females, pregnant_female);
	}

	for(int l=0; l<SIZE_EIGEN_VECTOR; l++){
		file >> nb_females;
		if(l<Abstract_Gestating_Group::nb_lists)
			fattened_cows->add_fattened_females_and_generate_dates(0, l, nb_females, empty_female);
	}

	for(int l=0; l<SIZE_EIGEN_VECTOR; l++){
		file >> nb_females;
		if(l<Abstract_Gestating_Group::nb_lists)
			fattened_cows->add_fattened_females_and_generate_dates(0, l, nb_females, pregnant_female);
	}

	for(int l=0; l<SIZE_EIGEN_VECTOR; l++){
		file >> nb_females;
		if(l<Abstract_Gestating_Group::nb_lists)
			fattened_empty->add_fattened_females_and_generate_dates(0, l, nb_females, empty_female);
	}

	unsigned int nb_S_bulls, nb_P_bulls;
	file >> nb_S_bulls >> nb_P_bulls;
	for(unsigned int q=0; q<nb_P_bulls; q++) bulls->replace_one_bull_by_a_P_bull();

	getline(file, line);
	getline(file, line);
	cout << "population herd have been initialized from a file" << endl;
}

void Herd::init_population(){
    Parameters& iParams = Parameters::getInstance();
	grassers->add_animals(Grassers_Group::S_, objective_weaned_calves - nb_heifers_for_renewal);
	young_heifers_one_year->add_animals(Young_Heifers_Group::S_, nb_heifers_for_renewal);
	young_heifers_two_years->add_animals(Young_Heifers_Group::S_, nb_heifers_for_renewal);
	bred_heifers->add_bred_heifers_and_generate_dates(0, Bred_Heifers_Group::SS_, iParams.breedHeifNb, undetermined);
	cows->add_cows_and_generate_dates(0, Cows_Group::SS_, iParams.breedCowsNb, undetermined);
}

void Herd::insert_infected_animal(const int week, const int type_of_introduction){
	switch(type_of_introduction){
	case BVDV_Introduction::P_Calves :
		if(week < calendar->calving_period.start)
			throw invalid_argument("group of Calves does not exist at that time");
		//calves->add_animals(Calves_Group::P_, 1);
        calves->replace_S_calve_by_P_calve(week);
		break;
	case BVDV_Introduction::P_Young_Heifers :
		young_heifers_one_year->add_animals(Young_Heifers_Group::P_, 1);
		break;
	case BVDV_Introduction::P_Pregnant_Heifers :
		if(calendar->indoor_period.start <= week && week < calendar->repro_heifers.start)
			throw invalid_argument("group of Bred Heifers does not exist at that time");
		bred_heifers->add_bred_heifers_and_generate_dates(week, Abstract_Gestating_Group::PP_, 1, pregnant_female);
		break;
	case BVDV_Introduction::P_Pregnant_Cows :
		cows->add_cows_and_generate_dates(week, Abstract_Gestating_Group::PP_, 1, pregnant_female);
		break;
	case BVDV_Introduction::RP_Bred_Heifers :
		if(calendar->indoor_period.start <= week && week < calendar->repro_heifers.start)
			throw invalid_argument("group of Bred Heifers does not exist at that time");
		bred_heifers->add_bred_heifers_and_generate_dates(week, Abstract_Gestating_Group::RP_, 1, pregnant_female);
		break;
	case BVDV_Introduction::RP_Cows :
		cows->add_cows_and_generate_dates(week, Abstract_Gestating_Group::RP_, 1, pregnant_female);
		break;
	case BVDV_Introduction::P_Bull_With_heifers :
		if(week < calendar->indoor_period.end || week > calendar->repro_heifers.end)
				throw invalid_argument("Bulls are not with Bred Heifers in pasture at that time");
		v_pastures[Pasture_Bred_Heifers].replace_one_bull_by_a_P_bull();
		break;
	case BVDV_Introduction::P_Bull_With_Cows :
		if(week < calendar->indoor_period.end || week > calendar->repro_cows.end)
				throw invalid_argument("Bulls are not with Cows in pasture at that time");
		v_pastures[Pasture_Calves_Cows].replace_one_bull_by_a_P_bull();
		break;
	case BVDV_Introduction::P_Bull :
		bulls->replace_one_bull_by_a_P_bull();
		break;
	}
}

void Herd::activate_Kext_and_purchase_of_infected_animals(const BVDV_Introduction& bvdv){
    Parameters& iParams = Parameters::getInstance();
    Kext = iParams.Kext;
    calves->set_proba_purchases(bvdv.proba_purchase_T_calves, bvdv.proba_purchase_P_calves);
    cows->set_proba_purchases(bvdv.proba_purchase_T_pregnant, bvdv.proba_purchase_P_pregnant,
            bvdv.proba_purchase_Rb_pregnant);
    bulls->set_proba_purchase_P_bull(bvdv.proba_purchase_P_bulls);
}

void Herd::desactivate_Kext_and_purchase_of_infected_animals(){
    Kext = 0.;
    calves->set_proba_purchases(0., 0.);
    cows->set_proba_purchases(0., 0., 0.);
    bulls->set_proba_purchase_P_bull(0.);
}

/*! \Warning: for simplicity and performance, bred heifers are not supposed to give birth */
void Herd::calvings(const int week){
	if(calendar->calving_period.start <= week && week <= calendar->calving_period.end){
		bred_heifers->calvings(week);
		cows->calvings(week);
		fattened_cows->calvings(week);
	}
}

void Herd::sell_grassers(const int week){
	grassers->sells_animals(week);
}

void Herd::purchase_calves(const int week){
	if(calendar->purchase_calves.start <= week && week <= calendar->purchase_calves.end)
		calves->purchase(week);
	// if one (or more) P pregnant female(s) died, we find nb_calvings_to_come > 0 at the end of calving period
	if(week == calendar->calving_period.end)
		calves->set_number_of_calvings_to_come(0);
}

void Herd::weaning(){
    Parameters& iParams = Parameters::getInstance();
	double nb_fattened_females_sold = fattened_cows->sell_all_females();
	Results::get_Instance()->add_data_year(Results::Sell_Fattened_Females_At_Weaning) = nb_fattened_females_sold;

	young_heifers_one_year->plus_one_year(young_heifers_two_years);

	int nb_calves_weaned = calves->get_nb_animals_in_this_group();
	if(nb_calves_weaned == 0)
		throw runtime_error("nb weaned calves = 0");

	int nb_females_calves = binomial(iParams.sexRatio, nb_calves_weaned);
	int nb_females_selected_for_renewal = min(nb_females_calves, nb_heifers_for_renewal);
	int nb_female_grassers = nb_females_calves - nb_females_selected_for_renewal;

	calves->transfer_to_group(young_heifers_one_year, nb_females_selected_for_renewal);
	calves->transfer_to_group(grassers);
	grassers->set_number_of_females(nb_female_grassers);

	Results::get_Instance()->add_data_year(Results::Weaning) = nb_calves_weaned;
}

void Herd::start_indoor_period(const int week){
    Explore::getInstance().setIsPasturePeriod(false);

	Results::get_Instance()->add_data_year(Results::Pregnant_Heif) = bred_heifers->get_number_of_pregnant_females();
	Results::get_Instance()->add_data_year(Results::Pregnant_Cows) = cows->get_number_of_pregnant_females();
	Results::get_Instance()->add_data_year(Results::Empty_Heif) = bred_heifers->get_number_of_empty_females();
	Results::get_Instance()->add_data_year(Results::Empty_Cows) = cows->get_number_of_empty_females();

	// all bred heifers are transfered in the cows group
	bred_heifers->transfer_to_cows(cows);

	// all empty females are transfered in the group of fattened females
	cows->transfer_empty_females_to_fattened_group(fattened_empty);

	// purchase of pregnant females if needed
	int nb_pregnant_cows = cows->get_nb_animals_in_this_group();
	unsigned int nb_females_purchased = (objective_pregnant_females > nb_pregnant_cows) ?
			objective_pregnant_females - nb_pregnant_cows : 0;
	if(nb_females_purchased > 0) cows->purchase_gestating_females(nb_females_purchased);
	Results::get_Instance()->add_data_year(Results::Purchase_Pregnant_Females) = nb_females_purchased;

	calves->set_number_of_calvings_to_come( cows->get_number_of_pregnant_females() );
}

void Herd::selling_empty_females(){
	fattened_empty->sell_all_females();
}

void Herd::start_repro_Heifers(const int week){
    Parameters& iParams = Parameters::getInstance();
	int nb_two_year_old_heifers = young_heifers_two_years->get_nb_animals_in_this_group();
	int nb_young_heifers_for_selling = max(0, nb_two_year_old_heifers - iParams.breedHeifNb);

	if (nb_young_heifers_for_selling > 0){
		young_heifers_two_years->transfer_to_bred_heifers(bred_heifers, week, iParams.breedHeifNb);
		young_heifers_two_years->sell_heifers();
	} else {
		young_heifers_two_years->transfer_to_bred_heifers(bred_heifers, week);
	}

	if (young_heifers_two_years->get_nb_animals_in_this_group() > 0)
		throw runtime_error("all 2-year-old heifers should be in bred heifers group or sold");

	Results::get_Instance()->add_data_year(Results::Sell_Heifers) = nb_young_heifers_for_selling;

    if (is_vaccination) bred_heifers->vaccinate();

    double pop = bred_heifers->get_nb_animals_in_this_group();
    Results::get_Instance()->add_data_year(Results::V1_Heif) = bred_heifers->get_nb_females_in_list(Abstract_Gestating_Group::V1M_) / pop;
    Results::get_Instance()->add_data_year(Results::V2_Heif) = bred_heifers->get_nb_females_in_list(Abstract_Gestating_Group::V2M_) / pop;
}

void Herd::start_repro_Cows(const int year){
    Parameters& iParams = Parameters::getInstance();
	int nb_cows = cows->get_nb_animals_in_this_group();
	if (nb_cows > iParams.breedCowsNb)
		cows->transfer_to_fattened_females(fattened_cows, nb_cows - iParams.breedCowsNb);
	//cows->generate_calving_dates_for_cows_that_have_already_calved();
	fattened_cows->replace_already_allocated_calving_dates_by_empty_dates(year);

	if (is_vaccination){
		switch (iParams.vaccinationScenario){
			case EveryYear :
				cows->vaccinate();
				break;
//			case EveryTwoYears : //are all breeding females vaccinated the first year ? To check...strange peak for new T the first year (almost 150)
//				if ((year - iParams.vaccStartYearNum) % 2 == 0) cows->vaccinate();
//				break;
		}
	}

	double pop = cows->get_nb_animals_in_this_group();
	Results::get_Instance()->add_data_year(Results::V1_Cows) = cows->get_nb_females_in_list(Abstract_Gestating_Group::V1M_) / pop;
	Results::get_Instance()->add_data_year(Results::V2_Cows) = cows->get_nb_females_in_list(Abstract_Gestating_Group::V2M_) / pop;
}

void Herd::start_pasture_period(){
    Parameters& iParams = Parameters::getInstance();
    Explore& oExplore = Explore::getInstance();
    if (iParams.ageGroupToInfect != -1 and not oExplore.getIsStabilisationPeriod()){
        v_groups[iParams.ageGroupToInfect]->moveSToP(iParams.nbToMoveFromSToP);
        v_groups[iParams.ageGroupToProtect]->moveSToR(iParams.propToMoveFromSToR);
    }

    oExplore.incrementYearNum();
    oExplore.setIsPasturePeriod(true);
    int femalesRnb = 0;
    int femalesTotNb = 0;
    int V1Nb = 0;
    int V2Nb = 0;
	for (Abstract_Group* group : v_gestating_groups){
		femalesRnb += group->get_nb_animals_in_health_state('R');
		femalesTotNb += group->get_nb_animals_in_this_group();
		V1Nb += group->get_nb_animals_in_health_state('1');
		V2Nb += group->get_nb_animals_in_health_state('2');
    }
    oExplore.add(Explore::eFemalesR, femalesRnb);
    oExplore.add(Explore::eFemalesTot, femalesTotNb);
    oExplore.add(Explore::eV1, V1Nb);
    oExplore.add(Explore::eV2, V2Nb);
    int totNb = 0;
    int indivPnb = 0;
    int RTotNb = 0;
    for (Abstract_Group* group : v_groups){
		totNb += group->get_nb_animals_in_this_group();
        indivPnb += group->get_nb_animals_in_health_state('P');
        RTotNb += group->get_nb_animals_in_health_state('R');
    }
    int STotNb = 0;
	for (auto i = 0; i < v_groups.size() - 1; ++i){
        //we don't want to take into account bulls, which can't be infected...but may be a better way to handle all these loops...
        STotNb += v_groups[i]->get_nb_animals_in_health_state('S');
    }
    oExplore.add(Explore::eTot, totNb);
    oExplore.add(Explore::eIndivP, indivPnb);
    oExplore.add(Explore::eRTot, RTotNb);
    oExplore.add(Explore::eSTotAtPastureStart, STotNb);

    int indexInExplore = 0;
	for (auto i = 0; i < v_groups.size() - 2; ++i){
        if (i == 2) continue;
        oExplore.addByGroup(Explore::eBG_Tot, indexInExplore, v_groups[i]->get_nb_animals_in_this_group());
        oExplore.addByGroup(Explore::eBG_STotAtPastureStart, indexInExplore, v_groups[i]->get_nb_animals_in_health_state('S'));
        oExplore.addByGroup(Explore::eBG_PTotAtPastureStart, indexInExplore, v_groups[i]->get_nb_animals_in_health_state('P'));
        oExplore.addByGroup(Explore::eBG_RTot, indexInExplore, v_groups[i]->get_nb_animals_in_health_state('R'));
        ++indexInExplore;
    }
    oExplore.addByGroup(Explore::eBG_Tot, Explore::eYoungHeifs, v_groups[2]->get_nb_animals_in_this_group());
    oExplore.addByGroup(Explore::eBG_STotAtPastureStart, Explore::eYoungHeifs, v_groups[2]->get_nb_animals_in_health_state('S'));
    oExplore.addByGroup(Explore::eBG_PTotAtPastureStart, Explore::eYoungHeifs, v_groups[2]->get_nb_animals_in_health_state('P'));
    oExplore.addByGroup(Explore::eBG_RTot, Explore::eYoungHeifs, v_groups[2]->get_nb_animals_in_health_state('R'));
    oExplore.addByGroup(Explore::eBG_Tot, Explore::eFattened, v_groups[7]->get_nb_animals_in_this_group());
    oExplore.addByGroup(Explore::eBG_STotAtPastureStart, Explore::eFattened, v_groups[7]->get_nb_animals_in_health_state('S'));
    oExplore.addByGroup(Explore::eBG_PTotAtPastureStart, Explore::eFattened, v_groups[7]->get_nb_animals_in_health_state('P'));
    oExplore.addByGroup(Explore::eBG_RTot, Explore::eFattened, v_groups[7]->get_nb_animals_in_health_state('R'));

	bulls->randomized_bulls();
	for(unsigned int i=0; i<nb_bulls_with_heifers; i++)
		v_pastures[Pasture_Bred_Heifers].assign_one_bull( bulls->get_bull(i) );
	for(unsigned int i=nb_bulls_with_heifers; i<nb_bulls; i++)
		v_pastures[Pasture_Calves_Cows].assign_one_bull( bulls->get_bull(i) );
}

void Herd::end_repro_period(){
	v_pastures[Pasture_Bred_Heifers].detach_all_bulls();
	v_pastures[Pasture_Calves_Cows].detach_all_bulls();
}

void Herd::bull_replacement(){
	bulls->replace_one_bull();
}

// when vaccinated females change from state V1 to V2
void Herd::start_progressive_loss_of_immunity(){
	if(calendar->is_OK_for_bred_heifers) bred_heifers->transfer_list_V1M_to_V2M();
	cows->transfer_list_V1M_to_V2M();
	fattened_cows->transfer_list_V1M_to_V2M();
	fattened_empty->transfer_list_V1M_to_V2M();
}

/*! \Warning method start_progressive_loss_of_immunity must be called before any vaccination */
void Herd::manage_special_dates(const int week){
	int year = week / Parameters::getInstance().weeksNbPerYear;

	if(week == calendar->progressive_loss_of_immunity)
		start_progressive_loss_of_immunity();

	if(week == calendar->weaning)				weaning();
	if(week == calendar->replacement_bull) 		bull_replacement();
	if(week == calendar->sell_empty)			selling_empty_females();

	if(week == calendar->indoor_period.start)	start_indoor_period(week);
	if(week == calendar->repro_heifers.start)	start_repro_Heifers(week);
	if(week == calendar->repro_cows.start)		start_repro_Cows(year);

	if(week == calendar->indoor_period.end)		start_pasture_period(); //Warning: because of this, indoor_period.end should be renamed pasture_period.start...
	if(week == calendar->repro_heifers.end)		end_repro_period();
}

void Herd::make_transfers_between_health_states(const int week){
	for(Abstract_Group* group : v_groups)
		group->make_transfers_between_health_states(week);
}

void Herd::calculate_transmission_force(const int week){
	// for the save_persistence method
	unsigned int total_T_animals = 0;
	unsigned int total_P_animals = 0;

	if(calendar->indoor_period.start <= week && week < calendar->indoor_period.end){
		calculate_transmission_force_in_homogeneous_pop(total_T_animals, total_P_animals);
	}else{
		calculate_transmission_force_in_inhomogeneous_pop(total_T_animals, total_P_animals, week);
        if(week >= calendar->repro_heifers.end || week < calendar->indoor_period.start)
		    total_P_animals += bulls->get_nb_animals_in_health_state('P');
	}

	save_persistence(total_T_animals, total_P_animals, week);
}

void Herd::save_persistence(unsigned int& total_T_animals, unsigned int& total_P_animals, int week){
	unsigned int total_RP_females = 0;
	for(Abstract_Gestating_Group* group : v_gestating_groups)
		total_RP_females += group->get_nb_females_in_list(Abstract_Gestating_Group::RP_);

	is_BVDV_present = (total_T_animals + total_P_animals + total_RP_females > 0 ) ? true : false;
	double persistence = (is_BVDV_present) ? 1.0 : 0.0;
	Results::get_Instance()->push_data_week(Results::Persistence, week, persistence, false);
}

void Herd::calculate_transmission_force_in_homogeneous_pop(unsigned int& total_T_animals, unsigned int& total_P_animals){
    Parameters& iParams = Parameters::getInstance();
	unsigned int total_population = 0;
	for(Abstract_Group* group : v_groups){
		total_population += group->get_nb_animals_in_this_group();
		total_T_animals += group->get_nb_animals_in_health_state('T');
		total_P_animals += group->get_nb_animals_in_health_state('P');
	}

	double transmission_force = 0.;
	if( total_T_animals + total_P_animals > 0 )
		transmission_force = (iParams.beta_T * total_T_animals + iParams.beta_P * total_P_animals) / static_cast<double>(total_population);
	for(Abstract_Group* group : v_groups) group->set_transmission_force(transmission_force);
}

void Herd::calculate_transmission_force_in_inhomogeneous_pop(unsigned int& total_T_animals, unsigned int& total_P_animals, const int week){
    Parameters& iParams = Parameters::getInstance();
	for(Pasture& pasture : v_pastures){
		pasture.calculate_total_T_and_P_animals();
		total_T_animals += pasture.nb_T_animals;		// to calculate persistence
		total_P_animals += pasture.nb_P_animals;		// to calculate persistence
	}

	double transmission_force = 0.;

	if( total_T_animals + total_P_animals > 0 || Kext > 1.e-10 ){
		for(unsigned int p=0; p<v_pastures.size(); p++){
			transmission_force = 0.;
			Pasture& pasture = v_pastures[p];
			if( pasture.nb_animals > 0){
				// from inside
				transmission_force = iParams.beta_T * pasture.nb_T_animals + iParams.beta_P * pasture.nb_P_animals;

				// contacts with PI animals from other pastures
				double contribution = 0.;
				for(unsigned int pprime=0; pprime<v_pastures.size(); pprime++){
					Pasture& other_pasture = v_pastures[pprime];
					if(pprime != p && other_pasture.nb_animals > 0 && v_delta_pastures[p][pprime])
						contribution += other_pasture.nb_P_animals / static_cast<double>(other_pasture.nb_animals);
				}
				transmission_force += iParams.beta * contribution;

				transmission_force /= static_cast<double>(pasture.nb_animals);

                if (not (calendar->indoor_period.start < iParams.weeksNbPerYear && week < calendar->indoor_period.start)){
				    if(Kext > 1.e-10 && v_delta_Kext[p]) transmission_force += Kext;
                }
			}
			pasture.set_transmission_force(transmission_force);
		}
	}else{
		for(Pasture& pasture : v_pastures) pasture.set_transmission_force(0.);
	}
}

// **************************************************************************************** //

void Herd::saveHeadcountPerStatePerGroupInFile(){
    string folderName = Parameters::getInstance().headcountFolder;
    string fileName;
    ofstream f;
	int g_pop = Results::Calves;
	int g_S = Results::S_Calves;
	int g_T = Results::T_Calves;
	int g_R = Results::R_Calves;
	int g_P = Results::P_Calves;
    int totNb = 0;
    int SNb = 0;
    int TNb = 0;
    int RNb = 0;
    int PNb = 0;
	for(Abstract_Group* group : v_groups){
        if (group == young_heifers_one_year){
            totNb = group->get_nb_animals_in_this_group();
            SNb = group->get_nb_animals_in_health_state('S');
            TNb = group->get_nb_animals_in_health_state('T');
            RNb = group->get_nb_animals_in_health_state('R');
            PNb = group->get_nb_animals_in_health_state('P');
        } else {
            fileName = folderName + "/" + Results::get_Instance()->get_name_entity(g_pop) + ".txt";
            f.open(fileName.c_str(), ofstream::app);
            f << group->get_nb_animals_in_this_group() + totNb << " ";
            f.close();
            fileName = folderName + "/" + Results::get_Instance()->get_name_entity(g_S) + ".txt";
            f.open(fileName.c_str(), ofstream::app);
            f << group->get_nb_animals_in_health_state('S') + SNb << " ";
            f.close();
            fileName = folderName + "/" + Results::get_Instance()->get_name_entity(g_T) + ".txt";
            f.open(fileName.c_str(), ofstream::app);
            f << group->get_nb_animals_in_health_state('T') + TNb << " ";
            f.close();
            fileName = folderName + "/" + Results::get_Instance()->get_name_entity(g_R) + ".txt";
            f.open(fileName.c_str(), ofstream::app);
            f << group->get_nb_animals_in_health_state('R') + RNb << " ";
            f.close();
            fileName = folderName + "/" + Results::get_Instance()->get_name_entity(g_P) + ".txt";
            f.open(fileName.c_str(), ofstream::app);
            f << group->get_nb_animals_in_health_state('P') + PNb << " ";
            f.close();
            g_pop++; g_S++; g_T++; g_R++; g_P++;
            totNb = 0;
            SNb = 0;
            TNb = 0;
            RNb = 0;
            PNb = 0;
        }
	}
    fileName = folderName + "/V1_Heif.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << bred_heifers->get_nb_animals_in_health_state('1') << " ";
    f.close();
    fileName = folderName + "/V2_Heif.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << bred_heifers->get_nb_animals_in_health_state('2') << " ";
    f.close();
    fileName = folderName + "/V1_Cows.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << cows->get_nb_animals_in_health_state('1') << " ";
    f.close();
    fileName = folderName + "/V2_Cows.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << cows->get_nb_animals_in_health_state('2') << " ";
    f.close();
    fileName = folderName + "/V1_Fat_Cows.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << fattened_cows->get_nb_animals_in_health_state('1') << " ";
    f.close();
    fileName = folderName + "/V2_Fat_Cows.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << fattened_cows->get_nb_animals_in_health_state('2') << " ";
    f.close();
    fileName = folderName + "/V1_Fat_empty.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << fattened_empty->get_nb_animals_in_health_state('1') << " ";
    f.close();
    fileName = folderName + "/V2_Fat_empty.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << fattened_empty->get_nb_animals_in_health_state('2') << " ";
    f.close();
    fileName = folderName + "/M_Calves.txt";
    f.open(fileName.c_str(), ofstream::app);
	f << calves->get_nb_animals_in_health_state('M') << " ";
    f.close();
}

void Herd::save_T_P_RP_and_pop_per_group(const int week){
	double pop = 0.;
	int g_pop = Results::Calves;
	int g_S = Results::S_Calves;
	int g_T = Results::T_Calves;
	int g_R = Results::R_Calves;
	int g_P = Results::P_Calves;
	for (Abstract_Group* group : v_groups){
		pop += group->get_nb_animals_in_this_group();
		Results::get_Instance()->add_data_year(g_S) += group->get_nb_animals_in_health_state('S');
		Results::get_Instance()->add_data_year(g_T) += group->get_nb_animals_in_health_state('T');
		Results::get_Instance()->add_data_year(g_R) += group->get_nb_animals_in_health_state('R');
		Results::get_Instance()->add_data_year(g_P) += group->get_nb_animals_in_health_state('P');
		if (group != young_heifers_one_year){ //"if" only to cumulate young_heifers_one_year and young_heifers_two_years in young_heifers
			Results::get_Instance()->push_data_week(g_pop, week, pop);
			g_pop++; g_S++; g_T++; g_R++; g_P++;
			pop = 0.;
		}
	}

    int nbP = 0;
    int nbR = 0;
    int nbTot = 0;
	for (Abstract_Group* group : v_groups){
        nbP += group->get_nb_animals_in_health_state('P');
        nbR += group->get_nb_animals_in_health_state('R');
        nbTot += group->get_nb_animals_in_this_group();
    }
    int nbS = 0;
	for (auto i = 0; i < v_groups.size() - 1; ++i){
        //we don't want to take into account bulls, which can't be infected...but may be a better way to handle all these loops...
        nbS += v_groups[i]->get_nb_animals_in_health_state('S');
    }
    Explore::getInstance().addIfPasture(Explore::ePresWeekNbOfPAtPasture, nbP);
    Explore::getInstance().addIfPasture(Explore::ePresWeekNbOfTotAtPasture, nbTot);
    Explore::getInstance().add(Explore::ePresWeekNbOfP, nbP);
    Explore::getInstance().add(Explore::ePresWeekNbOfS, nbS);
    Explore::getInstance().add(Explore::ePresWeekNbOfTot, nbTot);
    Explore::getInstance().fillRnbPerWeek(week, nbR);
    Explore::getInstance().fillTotnbPerWeek(week, nbTot);
	Results::get_Instance()->push_data_week(Results::TotalPop, week, nbTot, false);

	Results::get_Instance()->add_data_year(Results::RP_Heif) += bred_heifers->get_nb_females_in_list(Abstract_Gestating_Group::RP_);
	Results::get_Instance()->add_data_year(Results::RP_Cows) += cows->get_nb_females_in_list(Abstract_Gestating_Group::RP_);

	if(week < calendar->indoor_period.start || calendar->indoor_period.end < week){ //should be indoor_period.end <= week...
		Results::get_Instance()->add_data_year(Results::Pasture_YH) += v_pastures[Pasture_Young_Heifers].nb_animals;
		Results::get_Instance()->add_data_year(Results::Pasture_BH) += v_pastures[Pasture_Bred_Heifers].nb_animals;
		Results::get_Instance()->add_data_year(Results::Pasture_CaCo) += v_pastures[Pasture_Calves_Cows].nb_animals;

		Results::get_Instance()->add_data_year(Results::P_pasture_YH) += v_pastures[Pasture_Young_Heifers].nb_P_animals;
		Results::get_Instance()->add_data_year(Results::P_pasture_BH) += v_pastures[Pasture_Bred_Heifers].nb_P_animals;
		Results::get_Instance()->add_data_year(Results::P_pasture_CaCo) += v_pastures[Pasture_Calves_Cows].nb_P_animals;
	}

	double data;
	data = bred_heifers->get_nb_females_in_list(Abstract_Gestating_Group::V1M_);
	Results::get_Instance()->push_data_week(Results::V1_Heif, week, data, false);
	data = bred_heifers->get_nb_females_in_list(Abstract_Gestating_Group::V2M_);
	Results::get_Instance()->push_data_week(Results::V2_Heif, week, data, false);

	data = cows->get_nb_females_in_list(Abstract_Gestating_Group::V1M_);
	Results::get_Instance()->push_data_week(Results::V1_Cows, week, data, false);
	data = cows->get_nb_females_in_list(Abstract_Gestating_Group::V2M_);
	Results::get_Instance()->push_data_week(Results::V2_Cows, week, data, false);
}

void Herd::save_and_reset_deaths(){
	unsigned int nb_P_animals_dead = calves->get_and_reset_nb_deaths();
	Results::get_Instance()->add_data_year(Results::Calve_P_Death) = nb_P_animals_dead;
	Results::get_Instance()->add_data_year(Results::Calve_Death) = calves->get_and_reset_nb_natural_deaths();

	for(Abstract_Group* group : v_groups)
		nb_P_animals_dead += group->get_and_reset_nb_deaths();
	Results::get_Instance()->add_data_year(Results::P_Death) = nb_P_animals_dead;
}

void Herd::save_and_reset_abortions(){
	Results::get_Instance()->add_data_year(Results::Abortion_Heif) = bred_heifers->get_and_reset_nb_abortions();
	Results::get_Instance()->add_data_year(Results::Abortion_Cows) = cows->get_and_reset_nb_abortions();
}

// **************************************************************************************** //
void Herd::print_health_state(const char state, const bool is_header){
	if(is_header){
		cout << "state" << "\t" << "Ca" << "\t" << "Gr" << "\t" << "Yh" << "\t" <<
				"Bh" << "\t" << "Co" << "\t" << "FaC" << "\t" << "FaE"  << endl;
	}

	cout << state << "\t";

	if(state == 'M') cout << calves->get_nb_animals_in_health_state(state);

	if(state=='D' || state=='S' || state=='T' || state=='R' || state=='P'){
		cout << calves->get_nb_animals_in_health_state(state) << "\t";
		cout << grassers->get_nb_animals_in_health_state(state) << "\t";
		cout << young_heifers_one_year->get_nb_animals_in_health_state(state) +
				young_heifers_two_years->get_nb_animals_in_health_state(state) << "\t";
	}

	if(state == 'V') cout << " " << "\t" << " " << "\t" << " " << "\t";
	if(state=='D' || state=='S' || state=='T' || state=='R' || state=='P' || state=='V'){
		cout << bred_heifers->get_nb_animals_in_health_state(state) << "\t";
		cout << cows->get_nb_animals_in_health_state(state) << "\t";
		cout << fattened_cows->get_nb_animals_in_health_state(state) << "\t";
		cout << fattened_empty->get_nb_animals_in_health_state(state) << "\t";
	}
	cout << endl;
}

void Herd::print_herd_population(){
	vector<char> v_states = {'S', 'T', 'R', 'P', 'V', 'D'};
	print_health_state('M', true);
	for(char state : v_states) print_health_state(state);
	cout << "\n" << endl;
}

void Herd::print_dates_of_list(const int list){
	bred_heifers->print_dates_of_list(list, "bred heifers dates: ");
	cows->print_dates_of_list(list, "cows dates: ");
	fattened_cows->print_dates_of_list(list, "fattened cows dates: ");
	fattened_empty->print_dates_of_list(list, "fattened empty females: ");
	cout << "\n" << endl;
}

void Herd::print_delta(){
	cout << "delta Kext: " << endl;
	for(bool b : v_delta_Kext) cout << b << " ";
	cout << endl;

	cout << "delta between pasture: " << endl;
	for(vector<bool> vec : v_delta_pastures){
		for(bool b : vec) cout << b << " ";
		cout << endl;
	}
	cout << endl;
}

}
