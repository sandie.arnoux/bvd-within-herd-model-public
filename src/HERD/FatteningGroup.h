
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractGestatingGroup.h"

namespace model {

class Fattening_Group: public model::Abstract_Gestating_Group {
public:
	Fattening_Group(Herd_Calendar* calendar, Calves_Group* calves_group) :
		Abstract_Gestating_Group(calendar, calves_group, false){ }

	virtual ~Fattening_Group(){ }

	void add_fattened_females_and_generate_dates(const int week, const int list, const unsigned int nb_new_females, const enum_fertility type){
		add_females_and_generate_dates(week, list, nb_new_females, true, type);
	}

	unsigned int sell_all_females(){
		unsigned int nb_females_sold = get_nb_animals_in_this_group();
		sell_animals();
		return nb_females_sold;
	}

	void replace_already_allocated_calving_dates_by_empty_dates(const int year){
        Parameters& iParams = Parameters::getInstance();
		list_calving_dates tmp_list;
		for(list_calving_dates& list : lists){
			tmp_list.swap(list);
			for(int date : tmp_list)
				list.push_back( (date / iParams.weeksNbPerYear == year) ? date : iParams.emptyDate);
			tmp_list.clear();
		}
	}

	void make_transfers_between_health_states(const int week){
		Abstract_Gestating_Group::make_transfers_between_health_states(week);
        int nbT = transfMan->get_sum_transfers_to(TR_);
        Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eFattened, nbT);
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eFattened, nbT);
    }
};

}
