
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractGestatingGroup.h"
#include "FatteningGroup.h"

namespace model {

class Cows_Group: public model::Abstract_Gestating_Group {
public:
	Cows_Group(Herd_Calendar* calendar, Calves_Group* calves_group) :
		Abstract_Gestating_Group(calendar, calves_group, true){
        coverage_vaccination = Parameters::getInstance().vaccCowsCoverage;
        set_proba_purchases(0, 0, 0);
    }

	virtual ~Cows_Group(){ }

	void set_proba_purchases(const double proba_T, const double proba_P, const double proba_Rb){
		if( proba_T + proba_P + proba_Rb > 1.0)
			throw invalid_argument("sum of probabilities is greater than 1");
		reset_proba_purchase();
		add_proba_purchase(SS_, 1 - proba_T - proba_P - proba_Rb);
		add_proba_purchase(TR_, proba_T);
		add_proba_purchase(PP_, proba_P);
		add_proba_purchase(RP_, proba_Rb);
	}

	void add_cows_and_generate_dates(const int week, const int list, const unsigned int nb_new_cows, const enum_fertility type){
		add_females_and_generate_dates(week, list, nb_new_cows, true, type);
	}

	void transfer_to_fattened_females(Fattening_Group* fattened_females_group, const unsigned int nb_cows_for_transfer=ALL){
		array_lists selection;
		if(nb_cows_for_transfer == ALL){
			selection = select_and_remove_all();
		}else{
			selection = random_select_and_remove(nb_cows_for_transfer);
		}

		fattened_females_group->add_females_with_dates(selection);
	}

	void transfer_empty_females_to_fattened_group(Fattening_Group* fattened_females_group){
		transfer_empty_females_to_group(fattened_females_group);
	}

	void make_transfers_between_health_states(const int week){
		Abstract_Gestating_Group::make_transfers_between_health_states(week);
        int nbT = transfMan->get_sum_transfers_to(TR_);
        Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eCows, nbT);
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eCows, nbT);
    }
};

}
