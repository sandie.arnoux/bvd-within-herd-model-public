
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractNonGestatingGroup.h"
#include <boost/random/triangle_distribution.hpp>

namespace model {

class Grassers_Group: public model::Abstract_Non_Gestating_Group {
public:
	enum season_for_selling_grassers{
		Autumn_, Winter_, Spring_
	};

private:
	int period = 0;
	const static unsigned int nb_selling_periods = 3;
	const static int range_of_selling_periods = 6;
	array<double, nb_selling_periods> a_proportions_sells_for_males { {1., 0., 0.} };
	array<double, nb_selling_periods> a_proportions_sells_for_females { {1., 0., 0.} };
	array<int, nb_selling_periods> a_sell_dates { {10000, 10000, 10000} };
	array<int, nb_selling_periods> a_center_of_selling_periods { {10000, 10000, 10000} };

	unsigned int nb_female_grassers = 0;
	vector<unsigned int> v_states = {S_, T_, R_, P_};

	void update_selling_periods(const int year){
		a_center_of_selling_periods[Autumn_] = calendar->convert_to_week_according_to_weaning(15, Herd_Calendar::Nov_, year);
		a_center_of_selling_periods[Winter_] = calendar->convert_to_week_according_to_weaning(15, Herd_Calendar::Feb_, year);
		a_center_of_selling_periods[Spring_] = calendar->convert_to_week_according_to_weaning(15, Herd_Calendar::Jun_, year);

		for(int date : a_center_of_selling_periods){
			date %= Parameters::getInstance().weeksNbPerYear;;
			if(date - range_of_selling_periods/2 < 0 || date + range_of_selling_periods/2 > 51)
				throw runtime_error("period of sales for grassers out the range of a year [0, 52[ : " +
						             to_string(range_of_selling_periods/2) + " " + to_string(date));
		}
	}

	void set_proportions_sales(){
        Parameters& iParams = Parameters::getInstance();
        // renormalization of probabilities => renormalized_proba[i] = proba[i] / sum^N_{j=i} proba[j]
		a_proportions_sells_for_males[Autumn_] = iParams.malesSoldAtAutumnProp;
		a_proportions_sells_for_males[Winter_] = iParams.malesSoldAtWinterProp / (iParams.malesSoldAtWinterProp + iParams.malesSoldAtSpringProp);
		a_proportions_sells_for_males[Spring_] = (iParams.malesSoldAtSpringProp < 0.00001) ? 0.0 : 1.0;

		a_proportions_sells_for_females[Autumn_] = iParams.femalesSoldAtAutumnProp;
		a_proportions_sells_for_females[Winter_] = iParams.femalesSoldAtWinterProp / (iParams.femalesSoldAtWinterProp + iParams.femalesSoldAtSpringProp);
		a_proportions_sells_for_females[Spring_] = (iParams.femalesSoldAtSpringProp < 0.00001) ? 0.0 : 1.0;
	}

public:
	Grassers_Group(Herd_Calendar* calendar) : Abstract_Non_Gestating_Group(calendar){
		nb_health_states = P_;
		init_transfers();
		update_selling_periods(0);
		set_proportions_sales();
	}

	virtual ~Grassers_Group(){ }

	void set_number_of_females(const unsigned int nb_females){ nb_female_grassers = nb_females; }

	void make_transfers_between_health_states(const int week){
		Abstract_Non_Gestating_Group::make_transfers_between_health_states(week);
        int nbT = transfMan->get_transfer(S_, T_);
        Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eGrassers, nbT);
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eGrassers, nbT);
		// check if the dead grassers was a female
		for(unsigned int q=0; q<transfMan->get_transfer(P_, D_); q++)
			if(nb_female_grassers > 0 && uniform_int(0, get_nb_animals_in_this_group()) < nb_female_grassers) nb_female_grassers--;
	}

	/*! \Warning must be called at the beginning of each year */
	void calculate_new_dates_for_sales(const int year){
		period = 0;
		update_selling_periods(year);
		for(unsigned int p=0; p<nb_selling_periods; p++)
			a_sell_dates[p] = triangular(a_center_of_selling_periods[p], range_of_selling_periods);
	}

	void sells_animals(int week){
		if(week == a_sell_dates[period]){
			unsigned int nb_grassers = get_nb_animals_in_this_group();
			unsigned int nb_male_grassers = nb_grassers - nb_female_grassers;

			unsigned int nb_males_sold = ceil(a_proportions_sells_for_males[period] * nb_male_grassers);
			unsigned int nb_females_sold = ceil(a_proportions_sells_for_females[period] * nb_female_grassers);
			nb_female_grassers -= nb_females_sold;

			if(nb_males_sold + nb_females_sold == nb_grassers){
				empty_this_group();
			}else{
				random_select_and_remove(v_states, nb_males_sold + nb_females_sold);
			}

			period++;
			Results::get_Instance()->add_data_year(Results::Sell_Male_Grassers) += nb_males_sold;
			Results::get_Instance()->add_data_year(Results::Sell_Female_Grassers) += nb_females_sold;
		}
	}
};

}
