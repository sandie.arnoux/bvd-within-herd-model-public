
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractNonGestatingGroup.h"
#include "BredHeifersGroup.h"

namespace model {

// Before, only P was split into 2 compartments but it was very difficult to manage and has led to some bugs.
// That's why I finally decided to create 2 groups in the Herd class.
class Young_Heifers_Group: public model::Abstract_Non_Gestating_Group {
private:
	vector<unsigned int> v_states = {S_, T_, R_, P_};

public:
	Young_Heifers_Group(Herd_Calendar* calendar) : Abstract_Non_Gestating_Group(calendar){
		nb_health_states = P_;
		init_transfers();
	}

	virtual ~Young_Heifers_Group(){ }

	void plus_one_year(Young_Heifers_Group* two_years_old_heifers_group){
			Vector_UI v_selection = select_and_remove_all(v_states);
			two_years_old_heifers_group->add_animals(v_selection);
	}

	void sell_heifers(const unsigned int nb_heifers_to_sell=ALL){
		vector<unsigned int> v_states = {S_, T_, R_, P_};
		if(nb_heifers_to_sell == ALL){
			select_and_remove_all(v_states);
		}else{
			if(nb_heifers_to_sell > get_nb_animals_in_this_group())
				throw runtime_error("number of heifers to sell > number of heifers in the group");
			random_select_and_remove(v_states, nb_heifers_to_sell);
		}
	}

	void transfer_to_bred_heifers(Bred_Heifers_Group* bred_heifers_group, const int week, const unsigned int nb_heifers_for_transfer=ALL){
		Vector_UI v_selection;
		vector<unsigned int> v_states = {S_, T_, R_, P_};
		if(nb_heifers_for_transfer == ALL){
			v_selection = select_and_remove_all(v_states);
		}else{
			if(nb_heifers_for_transfer > get_nb_animals_in_this_group())
				throw runtime_error("nb heifers for transfer > nb 2-year-old heifers");
			v_selection = random_select_and_remove(v_states, nb_heifers_for_transfer);
		}

		map<int, int> map_states_lists = {{S_, Bred_Heifers_Group::SS_}, {T_, Bred_Heifers_Group::TR_},
				{R_, Bred_Heifers_Group::RM_}, {P_, Bred_Heifers_Group::PP_} };
		for(auto state_list : map_states_lists)
			bred_heifers_group->add_bred_heifers_and_generate_dates(week, state_list.second, v_selection(state_list.first), undetermined);
	}

	void make_transfers_between_health_states(const int week){
		Abstract_Non_Gestating_Group::make_transfers_between_health_states(week);
        int nbT = transfMan->get_transfer(S_, T_);
        Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eYoungHeifs, nbT);
        Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eYoungHeifs, nbT);
    }

};

}
