
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "CalvesGroup.h"
#include "GrassersGroup.h"
#include "YoungHeifersGroup.h"
#include "BredHeifersGroup.h"
#include "CowsGroup.h"
#include "FatteningGroup.h"
#include "Bulls.h"
#include "Pasture.h"

#include "../BVDVIntroduction.h"
#include "../RESULTS/Results.h"

namespace model {

class Herd {
public:
	enum names_pasture{
		Pasture_Grassers, Pasture_Young_Heifers, Pasture_Bred_Heifers, Pasture_Calves_Cows
	};

    enum scenario_vacc{
    	NoVaccination, EveryYear, EveryTwoYears, AllSCowsAfterFirstYear
    };

private:
	Herd_Calendar*        		calendar;

	Calves_Group*         		calves;
	Grassers_Group*       		grassers;
	Young_Heifers_Group*  		young_heifers_one_year;
	Young_Heifers_Group*  		young_heifers_two_years;
	Bred_Heifers_Group*   		bred_heifers;
	Cows_Group*           		cows;
	Fattening_Group*      		fattened_cows;
	Fattening_Group*			fattened_empty;
	Bulls_Group*		  		bulls;
	vector<Abstract_Group*>		v_groups;
	vector<Abstract_Gestating_Group*> v_gestating_groups;

	vector<Pasture> 			v_pastures;
	vector<bool>				v_delta_Kext;
	vector<vector<bool>>		v_delta_pastures;

	bool is_BVDV_present = false;

	bool is_vaccination = false;

	// fixed at beginning of the simulation (in constructor)
	unsigned int nb_bulls;
	unsigned int nb_bulls_with_heifers;
	unsigned int nb_bulls_with_cows;
	int nb_heifers_for_renewal;
	int objective_pregnant_females;
	int objective_weaned_calves;

	double Kext = 0.;

    void save_persistence(unsigned int& total_T_animals, unsigned int& total_P_animals, int week);

    /*! homogeneous :
     *  \f[ f_g = \frac{\beta_P \sum N^P_g + \beta_T \sum N^T_g}{\sum N_g} \f] */
    void calculate_transmission_force_in_homogeneous_pop(unsigned int& total_T_animals, unsigned int& total_P_animals);

    /*! inhomogeneous :
     *  \f[ f_p = \beta_P \frac{N^P_p}{N_p} + \beta_T \frac{N^T_p}{N_p} +
     *      \frac{\beta}{N_p} \left( \frac{K_{ext}}{N_p} + \sum_{p\prime\neq p} \frac{N^P_p\prime}{N_p\prime} \right) \f]*/
    void calculate_transmission_force_in_inhomogeneous_pop(unsigned int& total_T_animals, unsigned int& total_P_animals, const int week);

public:
	Herd();

	virtual ~Herd();

	void reset();

	void print_herd(ofstream& file, const int repetition);

    /*! We assumed that we are at week 0, just after the weaning */
    void init_population();

	void initialize_population_from_file(ifstream& file);

	bool is_BVDV_present_in_the_herd() const {
        return is_BVDV_present;
    }

	void activate_vaccination(){
		is_vaccination = true;
	}

	void desactivate_vaccination(){
		is_vaccination = false;
	}

	void set_delta_Kext(const unsigned int pasture){
		if(pasture > v_pastures.size())
			throw invalid_argument("pasture does not exist");
		v_delta_Kext[pasture] = true;
	}

	void set_delta_pastures(const unsigned int pasture_1, const unsigned int pasture_2){
		if(pasture_1 > v_pastures.size() || pasture_2 > v_pastures.size())
			throw invalid_argument("pasture does not exist");
		v_delta_pastures[pasture_1][pasture_2] = true;
		v_delta_pastures[pasture_2][pasture_1] = true;
	}

	void activate_Kext_and_purchase_of_infected_animals(const BVDV_Introduction& bvdv);

	void desactivate_Kext_and_purchase_of_infected_animals();

	void insert_infected_animal(const int week, const int type_of_introduction);

	// apply modulo 52 on all calving dates
	void shift_to_year_0(){
		calendar->shift_to_year_0();
		for(Abstract_Gestating_Group* group : v_gestating_groups)
			group->shift_calving_dates_to_year_0();
	}

	void plus_one_year(){
		calendar->plus_one_year();
	}

	void prepare_dates_to_sell_grassers(const int year){
		grassers->calculate_new_dates_for_sales(year);
	}

	//*********************************************************************//
	/*! \Warning we assumed that bred heifers are merged with cows before calvings */
	void calvings(const int week);

	void sell_grassers(const int week);

	void purchase_calves(const int week);

	/*! - calves are put either in \b Young_Heifers, or in \b Grassers
	 *  + all male calves are put in \b Grassers
	 *  + a fixed number of female calves are put in \b Young_Heifers, the others in Grassers
	 *  - all cows in \b Fattening are sold */
    void weaning();

	/*! Empty females are put in \b Fattening, pregnant in \b Cows.\n
	 *  Pregnant females are purchased if needed. */
    void start_indoor_period(const int week);

	/*! All empty animals in \b Fattening are sold
	 * (occurs 100 days after the beginning of the indoor period) */
    void selling_empty_females();

	/*! - A fixed number of 2 years old young heifers are selected for breeding.
	 *  The remaining heifers of two years old are sold.\n
	 *  - Breeding heifers are vaccinated */
    void start_repro_Heifers(const int week);

	/*! - A fixed number of cows are selected for breeding, the others are fattened
	 *  until they are sold at next weaning.\n
	 *  - Breeding cows are vaccinated
	 *  \attention cows in Fattening are NOT vaccinated */
    void start_repro_Cows(const int year);

    /*! Random repartition of bulls between Bred Heifers and Cows
     *  \note only one bull with Heifers */
	void start_pasture_period();

	/*! Bulls are separated from the breeding females */
	void end_repro_period();

	/*! Annual replacement of one bull selected randomly among all bulls */
    void bull_replacement();

    /*! Vaccinated females will begin to lose progressively their immunity */
    void start_progressive_loss_of_immunity();

    void manage_special_dates(const int week);

    void make_transfers_between_health_states(const int week);

    void calculate_transmission_force(const int week);

    //*********************************************************************//
    void saveHeadcountPerStatePerGroupInFile();

    void save_T_P_RP_and_pop_per_group(const int week);

    void save_and_reset_deaths();

    void save_and_reset_abortions();

    //*************************** for debug *******************************//
    void print_health_state(const char state, const bool is_header=false);

    void print_herd_population();

    void print_dates_of_list(const int list);
    
    void print_delta();
};

}
