
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "../random_distributions.h"
#include "AbstractGroup.h"

namespace model {

class Bull{
private:
	int number;
	bool is_bull_P = false;
	double proba_purchase_P_bull = 0.0;
	double proba_death;

public:
	Bull(const int number) : number(number) {
        Parameters& iParams = Parameters::getInstance();
        proba_death = 1. - exp(- iParams.mu_P * iParams.daysNbPerTimeStep);
    }
	~Bull(){ }

	void reset(){ is_bull_P = false; }

	void set_proba_purchase_P_bull(const double proba){ proba_purchase_P_bull = proba; }

	double get_proba_purchase_P_bull(){ return proba_purchase_P_bull; }

	bool is_P(){ return is_bull_P; }

	void replace_by_a_P_bull(){ is_bull_P = true; }

	void replace_bull(){
		 is_bull_P = bernoulli(proba_purchase_P_bull);
		 if(is_bull_P){
			 Results::get_Instance()->add_data_year(Results::New_P)++;
             Explore::getInstance().add(Explore::eNewP, 1);
			 Results::get_Instance()->v_nb_infected_animal_purchased[Results::Bull_P]++;
		 }
		 Results::get_Instance()->add_data_year(Results::Purchase_Bulls)++;
	}

	unsigned int check_if_dead_and_replace(){
		if(is_bull_P){
			bool is_dead = bernoulli(proba_death);
			if(is_dead) replace_bull();
			return (is_dead) ? 1 : 0;
		}else{
			return 0;
		}
	}

};

class Bulls_Group : public Abstract_Group{
private:
	unsigned int nb_bulls;
	vector<Bull> v_bulls;
	unsigned int nb_deaths = 0;

public:
	Bulls_Group(Herd_Calendar* calendar, const unsigned int nb_bulls) : Abstract_Group(calendar), nb_bulls(nb_bulls){
		for(unsigned int i=0; i<nb_bulls; i++) v_bulls.push_back( Bull(i) );
	}
	~Bulls_Group(){ }

	void reset(){
		for(Bull& bull : v_bulls) bull.reset();
	}

	void set_proba_purchase_P_bull(const double proba){
		if(proba > 1.) throw invalid_argument("probability associated with the purchase of P bulls must not exceed <= 1.");
		for(Bull& bull : v_bulls) bull.set_proba_purchase_P_bull(proba);
	}

	void init_transfers(){ }

	unsigned int get_nb_animals_in_this_group(){ return nb_bulls; }

	unsigned int get_and_reset_nb_deaths(){
		unsigned int tmp_nb_deaths = nb_deaths;
		nb_deaths = 0;
		return tmp_nb_deaths;
	}

	unsigned int get_nb_animals_in_health_state(const char state){
		unsigned int nb_animals = 0;
		switch(state){
			case 'S' :
				nb_animals = nb_bulls;
				for(Bull& bull : v_bulls) if( bull.is_P() ) nb_animals--;
				break;
			case 'P':
				for(Bull& bull : v_bulls) if( bull.is_P() ) nb_animals++;
				break;
			default:
				break;
		}
		return nb_animals;
	}

	void set_transmission_force(const double force){ }

	void empty_this_group(){ }

	void make_transfers_between_health_states(const int week){
		for(Bull& bull : v_bulls) nb_deaths += bull.check_if_dead_and_replace();
	}

	void replace_one_bull_by_a_P_bull(){
		int position = uniform_int(0, nb_bulls);
		v_bulls[position].replace_by_a_P_bull();
	}

	void replace_one_bull(){
		int position = uniform_int(0, nb_bulls);
		v_bulls[position].replace_bull();
	}

	void randomized_bulls(){
        Parameters& iParams = Parameters::getInstance();
        std::random_device rd;
        std::mt19937 gen(rd());
		shuffle(v_bulls.begin(), v_bulls.end(), gen);
	}

	Bull* get_bull(const int position){
		return &v_bulls[position];
	}
};

}
