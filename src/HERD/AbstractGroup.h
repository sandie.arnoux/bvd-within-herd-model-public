
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "../includes.h"
#include "../random_distributions.h"
#include "../MANAGERS/HerdCalendar.h"
#include "../RESULTS/Results.h"

namespace model {

class Abstract_Group {
protected:
	vector<unsigned int> v_state_purchase;
	vector<double> v_proba_purchase;
	Herd_Calendar* calendar;

	void reset_proba_purchase(){
		v_state_purchase.clear();
		v_proba_purchase.clear();
	}

	void add_proba_purchase(const unsigned int state, const double probability){
		v_state_purchase.push_back(state);
		v_proba_purchase.push_back(probability);
	}

	unsigned int get_state_of_animal_purchased(){
        Parameters& iParams = Parameters::getInstance();
		boost::random::discrete_distribution<> state_generator(v_proba_purchase.begin(), v_proba_purchase.end());
		return v_state_purchase[state_generator(iParams.gen)];
	}

public:
	Abstract_Group(Herd_Calendar* calendar){
		this->calendar = calendar;
	}

	virtual ~Abstract_Group(){}

	virtual void init_transfers() =0;

	virtual unsigned int get_nb_animals_in_this_group() =0;

	virtual unsigned int get_and_reset_nb_deaths() =0;

	virtual unsigned int get_nb_animals_in_health_state(const char state) =0;

	virtual void set_transmission_force(const double force) =0;

	virtual void empty_this_group() =0;

	virtual void make_transfers_between_health_states(const int week) =0;

    virtual void moveProportionFromSStoRR(){}

    virtual void moveSToP(int nb){}
    virtual void moveSToR(float prop){}
};

}
