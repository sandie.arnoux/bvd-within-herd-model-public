
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractGroup.h"
#include "../MANAGERS/TransferComps.h"
#include <random>

namespace model {

class Abstract_Non_Gestating_Group: public model::Abstract_Group {
public:
	enum enum_compartments{
		D_, S_, T_, R_, P_
	};

protected:
	Vector_UI v_compartments;
	Transfer_Comps* transfMan;

	unsigned int purchase_animals(unsigned int nb_animals){
		unsigned int nb_P_purchased = 0;
        unsigned int nb_T_purchased = 0;
        unsigned int nb_S_purchased = 0;
        unsigned int state;
		for (unsigned int i = 1; i <= nb_animals; i++){
			state = get_state_of_animal_purchased();
			v_compartments( state )++;
			if (state == T_) nb_T_purchased++;
			if (state == P_) nb_P_purchased++;
			if (state == S_) nb_S_purchased++;
        }
		if (nb_T_purchased > 0){
            Results::get_Instance()->add_data_year(Results::New_T) += nb_T_purchased;
            Explore::getInstance().add(Explore::eNewT, nb_T_purchased);
            Explore::getInstance().addIfPasture(Explore::eNewTAtPasture, nb_T_purchased);
            Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eCalves, nb_T_purchased);
            Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eCalves, nb_T_purchased);
        }
		if (nb_P_purchased > 0){
            Results::get_Instance()->add_data_year(Results::New_P) += nb_P_purchased;
            Explore::getInstance().add(Explore::eNewP, nb_P_purchased);
        }
		if (nb_S_purchased > 0){
            Explore::getInstance().add(Explore::eNewS, nb_S_purchased);
            Explore::getInstance().addIfPasture(Explore::eNewSAtPasture, nb_S_purchased);
            Explore::getInstance().addByGroup(Explore::eBG_NewS, Explore::eCalves, nb_S_purchased);
            Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewSAtPasture, Explore::eCalves, nb_S_purchased);
        }
		Results::get_Instance()->v_nb_infected_animal_purchased[Results::Calve_T] += nb_T_purchased;
		Results::get_Instance()->v_nb_infected_animal_purchased[Results::Calve_P] += nb_P_purchased;

		return nb_P_purchased;
	}

	void sell_animals(vector<unsigned int>& v_states, unsigned int nb_animals_to_sell=ALL){
		if(nb_animals_to_sell == ALL){
			select_and_remove_all(v_states);
		}else{
			random_select_and_remove(v_states, nb_animals_to_sell);
		}
	}

	Vector_UI select_and_remove_all(vector<unsigned int>& v_states){
		Vector_UI v_selection;
		for(unsigned int state : v_states){
			v_selection(state) = v_compartments(state);
			v_compartments(state) = 0;
		}
		return v_selection;
	}

	/*! Works as the french MOTUS tv game.
	 *  step 1 : For each state X, you put Nx balls numeroted X into a box.\n
	 *  step 2 : you shake the box.\n
	 *  step 3 : you pick up N balls and count the number of taken balls for each state. */
	Vector_UI random_select_and_remove(vector<unsigned int>& v_states, unsigned int nb_animals_to_select){
        Parameters& iParams = Parameters::getInstance();
		Vector_UI v_selection;
		vector<unsigned int> list;
		for(unsigned int state : v_states) list.insert(list.end(), v_compartments(state), state);
        std::random_device rd;
        std::mt19937 gen(rd());
        shuffle(list.begin(), list.end(), gen);
		for(unsigned int i=0; i<nb_animals_to_select; i++) v_selection( list[i] )++;
		v_compartments -= v_selection;
		return v_selection;
	}

public:
	int nb_health_states = 4;

	Abstract_Non_Gestating_Group(Herd_Calendar* calendar) : Abstract_Group(calendar){
		v_compartments.setZero();
		transfMan = new Transfer_Comps(v_compartments);
	}

	virtual ~Abstract_Non_Gestating_Group(){
		delete transfMan;
	}

	void init_population(const Vector_UI& init_vector){
		v_compartments = init_vector;
	}

	Vector_UI get_vector_population(){
		return v_compartments;
	}

	/*! initialization  of all transfers between compartments */
	void init_transfers(){
        Parameters& iParams = Parameters::getInstance();
		transfMan->add_new_transfer(S_, T_, 0.);
		transfMan->add_new_transfer(T_, R_, iParams.rate_T_R);
		transfMan->add_new_transfer(P_, D_, iParams.mu_P);
	}

	void set_transmission_force(const double force){
		transfMan->change_rate(S_, T_, force);
	}

	void make_transfers_between_health_states(const int week){
		transfMan->calculate_and_make_transfers();
        int nb = transfMan->get_transfer(S_, T_);
		Results::get_Instance()->add_data_year(Results::New_T) += nb;
        Explore::getInstance().add(Explore::eNewT, nb);
        Explore::getInstance().addIfPasture(Explore::eNewTAtPasture, nb);
	}

	unsigned int get_nb_animals_in_this_group(){
		return v_compartments.segment(S_, nb_health_states).sum();
	}

	void add_animals(unsigned int state, unsigned int nb_new_animals){
		v_compartments(state) += nb_new_animals;
	}

	void add_animals(Vector_UI& v_new_animals){
		v_compartments += v_new_animals;
	}

    void moveSToP(int nbToMove){
        int nbS = v_compartments(S_);
        if (nbS < nbToMove){
            //throw runtime_error("ERROR: try to move "+ to_string(nb) +" in P, but only "+ to_string(v_compartments(S_)) +" S");
            cout << "week " << Parameters::getInstance().weekNum << " WARNING: move only " << nbS << " P instead of " << nbToMove << endl;
            nbToMove = nbS;
        }
        v_compartments(S_) -= nbToMove;
        v_compartments(P_) += nbToMove;
    }

    void moveSToR(float prop){
        int nb = v_compartments(S_) * prop;
        v_compartments(S_) -= nb;
        v_compartments(R_) += nb;
    }

	unsigned int get_and_reset_nb_deaths(){
		unsigned int nb_deaths = v_compartments(D_);
		v_compartments(D_) = 0;
		return nb_deaths;
	}

	unsigned int get_nb_animals_in_health_state(const char state){
		switch(state){
			case 'S' :
				return v_compartments(S_);
				break;
			case 'T' :
				return v_compartments(T_);
				break;
			case 'R' :
				return v_compartments(R_);
				break;
			case 'P' :
				return v_compartments(P_);
				break;
			default:
				return 0;
		}
	}

	void empty_this_group(){
		v_compartments.segment(S_, nb_health_states).setZero();
	}
};

}
