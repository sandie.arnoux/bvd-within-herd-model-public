
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "AbstractGroup.h"
#include "CalvesGroup.h"
#include "../MANAGERS/GenerateCalvingDates.h"
#include "../MANAGERS/TransferLists.h"
#include <random>

namespace model {

class Abstract_Gestating_Group: public model::Abstract_Group, public model::Common_For_Lists {
protected:
	map<int, int> map_list_state_foetus = { {SS_, Calves_Group::S_}, {TR_, Calves_Group::R_},
			{RM_, Calves_Group::M_}, {RR_, Calves_Group::R_}, {RP_, Calves_Group::P_},
			{PP_, Calves_Group::P_}, {V1M_, Calves_Group::M_}, {V2M_, Calves_Group::M_} };

	array_lists lists;
	Generate_Calving_Dates* date_generator;
	Transfer_Lists* transfMan;
	Calves_Group* calves_group;
	bool is_breeding_females;

	unsigned int nb_deaths = 0;

	double rate_V_S = 1. / (6 * 30.5);

	// proportion of females that are vaccinated
	double coverage_vaccination = 0.;

    void add_females_with_dates(const int list, list_calving_dates& list_new_females){
    	lists[list].insert(lists[list].end(), list_new_females.begin(), list_new_females.end());
    	list_new_females.clear();
    }

	void add_females_and_generate_dates(const int week, const int list, const unsigned int nb_new_females, const bool isCow, const enum_fertility type){
		int calving_date;
		if (nb_new_females > 1000) throw invalid_argument("number of new females cannot exceed 1000");
		for (unsigned int q = 0; q < nb_new_females; q++){
			calving_date = date_generator->generate_new_calving_date(week, isCow, type);
			lists[list].push_back(calving_date);
		}
	}

	void add_females_and_generate_dates(const int week, array<unsigned int, nb_lists> a_new_females, const bool isCow, const enum_fertility type){
		for(int list=0; list<nb_lists; list++) add_females_and_generate_dates(week, list, a_new_females[list], isCow, type);
	}

	void sell_animals(unsigned int nb_animals_to_sell=ALL)
	{
		if(nb_animals_to_sell == ALL){
			empty_this_group();
		}else{
			random_select_and_remove(nb_animals_to_sell);
		}
	}

	void transfer_empty_females_to_group(Abstract_Gestating_Group* group){
        Parameters& iParams = Parameters::getInstance();
		unsigned int nb_empty;
		for(int list=0; list<nb_lists; list++){
			nb_empty = std::count(lists[list].begin(), lists[list].end(), iParams.emptyDate);
            lists[list].erase(remove(lists[list].begin(), lists[list].end(), iParams.emptyDate), lists[list].end());
			for(unsigned int q=0; q<nb_empty; q++) group->insert_one_date(list, iParams.emptyDate);
		}
	}

    list_calving_dates select_and_remove_dates(const int list, const unsigned int nb_dates=ALL){
    	list_calving_dates selection;
    	if(nb_dates == ALL){
    		selection.swap(lists[list]);
    	}else{
    		for(unsigned int q=0; q<nb_dates; q++) selection.push_back( select_and_remove_one_date(list) );
    	}
    	return selection;
    }

    array_lists select_and_remove_all(){
    	array_lists array_selection;
    	for(int list=0; list<nb_lists; list++) array_selection[list].swap(lists[list]);
    	return array_selection;
    }

    array_lists random_select_and_remove(unsigned int nb_females_to_select){
        Parameters& iParams = Parameters::getInstance();
    	int date;
    	array_lists array_selection;
		vector<int> vec;
		for(int list=0; list<nb_lists; list++) vec.insert(vec.end(), lists[list].size(), list);
        std::random_device rd;
        std::mt19937 gen(rd());
		shuffle(vec.begin(), vec.end(), gen);
		for(unsigned int i=0; i<nb_females_to_select; i++){
			date = select_and_remove_one_date(vec[i]);
			array_selection[ vec[i] ].push_back(date);
		}
		return array_selection;
    }

    void insert_one_date(const int list, const int date){
    	lists[list].push_back(date);
    }

    int select_and_remove_one_date(const int list){
    	return select_and_remove_one_date_from_list(lists[list]);
    }

	void set_partial_immunity_duration(){
        Parameters& iParams = Parameters::getInstance();
		rate_V_S = 1.0 / static_cast<double>(iParams.partialImmunityWeeksNb * iParams.daysNbPerWeek);
		transfMan->change_rate(V2M_, SS_, rate_V_S);
	}

	// protection against symptoms => females can be infected but they are not contagious and foetus is safe
	//                             => transition V -> R
	// transition V -> T is given by (1.-vaccine_efficiency) * (1.-protection_against_symptoms)
	// transition V -> R is given by (1.-vaccine_efficiency) * protection_against_symptoms
	void set_protection_against_symptoms(){
        double protection = Parameters::getInstance().vaccEfficiencyAgainstSymptoms;
		transfMan->change_weight(V1M_, TR_, abs(1. - protection));
		transfMan->change_weight(V1M_, RM_, protection);
		transfMan->change_weight(V2M_, TR_, abs(1. - protection));
		transfMan->change_weight(V2M_, RM_, protection);
	}

public:
	Abstract_Gestating_Group(Herd_Calendar* calendar, Calves_Group* calves_group, const bool is_breeding_females) :
		Abstract_Group(calendar), is_breeding_females(is_breeding_females){
		this->calves_group = calves_group;
		date_generator = new Generate_Calving_Dates(calendar);
		transfMan = new Transfer_Lists(lists, nb_deaths, date_generator);
		init_transfers();
        set_partial_immunity_duration();
        set_protection_against_symptoms();
	}

	virtual ~Abstract_Gestating_Group(){
		delete date_generator;
        delete transfMan;
	}

    /**** useful only for AllSCowsAfterFirstYear vaccination scenario...*****/
	void set_coverage_of_vaccinated_females(const double coverage){
		coverage_vaccination = coverage;
	}

	double get_coverage_of_vaccinated_females() const{
		return coverage_vaccination;
	}

	unsigned int get_and_reset_nb_deaths(){
		unsigned int tmp_nb_deaths = nb_deaths;
		nb_deaths = 0;
		return tmp_nb_deaths;
	}

	unsigned int get_and_reset_nb_abortions(){
		unsigned int nb_abortions = transfMan->get_nb_abortions();
		transfMan->reset_nb_abortions();
		return nb_abortions;
	}

	void init_transfers(){
        Parameters& iParams = Parameters::getInstance();
		transfMan->add_new_transfer(SS_, TR_, 0.);
		transfMan->add_new_transfer(TR_, RX_, iParams.rate_T_R);
		transfMan->add_new_transfer(PP_, DD_, iParams.mu_P);
		transfMan->add_new_transfer(V2M_, SS_, rate_V_S);

		transfMan->add_new_transfer(V1M_, TR_, 0.);
		transfMan->add_new_transfer(V1M_, RM_, 0.);
		transfMan->add_new_transfer(V2M_, TR_, 0.);
		transfMan->add_new_transfer(V2M_, RM_, 0.);
	}

	void set_transmission_force(double force){
		transfMan->change_rate(SS_, TR_, force);

		// transmission force for vaccinated females
		// efficiency of the vaccine => if < 1., females can be infected while they are vaccinated
		force *= abs(1. - Parameters::getInstance().vaccEfficiency);
		transfMan->change_rate(V1M_, TR_, force);
		transfMan->change_rate(V1M_, RM_, force);
		transfMan->change_rate(V2M_, TR_, force);
		transfMan->change_rate(V2M_, RM_, force);
	}

	void make_transfers_between_health_states(const int week){
		transfMan->calculate_and_make_transfers(week);
        
        int nb = transfMan->get_sum_transfers_to(TR_);
		Results::get_Instance()->add_data_year(Results::New_T) += nb;
        Explore::getInstance().add(Explore::eNewT, nb);
        Explore::getInstance().addIfPasture(Explore::eNewTAtPasture, nb);
	}

	unsigned int get_nb_animals_in_this_group(){
		unsigned int nb_females = 0;
		for(list_calving_dates& list : lists) nb_females += list.size();
		return  nb_females;
	}

	void empty_this_group(){
		for(list_calving_dates& list : lists) list.clear();
	}

	unsigned int get_nb_females_in_list(const int list){
		return lists[list].size();
	}

	unsigned int get_nb_animals_in_health_state(const char state){
		switch(state){
			case 'S' :
				return lists[SS_].size();
				break;
			case 'T' :
				return lists[TR_].size();
				break;
			case 'R' :
				return lists[RM_].size() + lists[RR_].size() + lists[RP_].size();
				break;
			case 'P' :
				return lists[PP_].size();
				break;
			case 'V' :
				return lists[V1M_].size() + lists[V2M_].size();
				break;
			case '1' :
				return lists[V1M_].size();
				break;
			case '2' :
				return lists[V2M_].size();
				break;
			default:
				return 0;
		}
	}

	unsigned int get_number_of_pregnant_females(){
		unsigned int nb_pregnant_females = 0;
		for(list_calving_dates& list : lists)
			nb_pregnant_females += ( list.size() - std::count(list.begin(), list.end(), Parameters::getInstance().emptyDate) );
		return nb_pregnant_females;
	}

	Vector_UI get_vector_pregnant_females(){
		Vector_UI vector;
		int l = 0;
		for(list_calving_dates& list : lists)
			vector(l++) = list.size() - std::count(list.begin(), list.end(), Parameters::getInstance().emptyDate);
		return vector;
	}

	unsigned int get_number_of_empty_females(){
		unsigned int nb_empty_females = 0;
		for(list_calving_dates& list : lists) nb_empty_females += std::count(list.begin(), list.end(), Parameters::getInstance().emptyDate);
		return nb_empty_females;
	}

	Vector_UI get_vector_empty_females(){
		Vector_UI vector;
		int l = 0;
		for(list_calving_dates& list : lists)
			vector(l++) = std::count(list.begin(), list.end(), Parameters::getInstance().emptyDate);
		return vector;
	}

	void purchase_gestating_females(unsigned int nb_animals){
		unsigned int nb_P_purchased = 0;
        unsigned int nb_T_purchased = 0;
        unsigned int nb_RP_purchased = 0;
        unsigned int nb_SS_purchased = 0;
		int list, date;
		for (unsigned int i = 1; i <= nb_animals; i++){
			list = get_state_of_animal_purchased();
			date = date_generator->generate_new_calving_date(calendar->indoor_period.start, true, pregnant_female);
			lists[list].push_back(date);
			if (list == TR_) nb_T_purchased++;
			if (list == PP_) nb_P_purchased++;
			if (list == RP_) nb_RP_purchased++;
			if (list == SS_) nb_SS_purchased++;
        }
		if (nb_T_purchased > 0){
            Results::get_Instance()->add_data_year(Results::New_T) += nb_T_purchased;
            Explore::getInstance().add(Explore::eNewT, nb_T_purchased);
            Explore::getInstance().addByGroup(Explore::eBG_NewT, Explore::eCows, nb_T_purchased);
            Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewTAtPasture, Explore::eCows, nb_T_purchased);
        }
		if (nb_P_purchased > 0){
            Results::get_Instance()->add_data_year(Results::New_P) += nb_P_purchased;
            Explore::getInstance().add(Explore::eNewP, nb_P_purchased);
        }
		if (nb_SS_purchased > 0){
            Explore::getInstance().add(Explore::eNewS, nb_SS_purchased);
            Explore::getInstance().addByGroup(Explore::eBG_NewS, Explore::eCows, nb_SS_purchased);
            Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewSAtPasture, Explore::eCows, nb_SS_purchased);
        }
		Results::get_Instance()->v_nb_infected_animal_purchased[Results::Female_T] += nb_T_purchased;
		Results::get_Instance()->v_nb_infected_animal_purchased[Results::Female_P] += nb_P_purchased;
		Results::get_Instance()->v_nb_infected_animal_purchased[Results::Female_RP] += nb_RP_purchased;
	}

	/*! all calving dates are shifted to correspond to year 0
	 *  \Warning Need to be used just after the stabilization period. */
	void shift_calving_dates_to_year_0(){
		list_calving_dates tmp_list;
		for(list_calving_dates& list : lists){
			tmp_list.swap(list);
			for(int date : tmp_list)
				list.push_back( (date > 0) ? date % Parameters::getInstance().weeksNbPerYear : date );
			tmp_list.clear();
		}
	}

    void add_females_with_dates(array_lists& lists_new_females){
    	for(int list=0; list<nb_lists; list++) add_females_with_dates(list, lists_new_females[list]);
    }

    /*! \Warning MUST be called before vaccinate() method */
    void transfer_list_V1M_to_V2M(){
    	add_females_with_dates(V2M_, lists[V1M_]);
    }

    void vaccinate_only_S_females(){
    	unsigned int nb_vaccinated_females;
		list_calving_dates list_of_vaccinated_females;

		// SS -> V1M
		nb_vaccinated_females = binomial(coverage_vaccination, lists[SS_].size());
		list_of_vaccinated_females = select_and_remove_dates(SS_, nb_vaccinated_females);
		add_females_with_dates(V1M_, list_of_vaccinated_females);
    }

	void vaccinate(){
		if(coverage_vaccination > 1.e-5){
			unsigned int nb_vaccinated_females;
			list_calving_dates list_of_vaccinated_females;

			// SS -> V1M
			nb_vaccinated_females = coverage_vaccination * lists[SS_].size();
			list_of_vaccinated_females = select_and_remove_dates(SS_, nb_vaccinated_females);
			add_females_with_dates(V1M_, list_of_vaccinated_females);

			// V2M -> V1M
			nb_vaccinated_females = coverage_vaccination * lists[V2M_].size();
			list_of_vaccinated_females = select_and_remove_dates(V2M_, nb_vaccinated_females);
			add_females_with_dates(V1M_, list_of_vaccinated_females);
		}
	}

    void moveProportionFromSStoRR(){
        unsigned int nbToMove;
      	list_calving_dates lDates;
        nbToMove = lists[SS_].size() * Parameters::getInstance().femalesRprop;
		lDates = select_and_remove_dates(SS_, nbToMove);
		add_females_with_dates(RR_, lDates);
    }

    void moveSToP(int nbToMove){
        int nbS = lists[SS_].size();
        if (nbS < nbToMove){
            //throw runtime_error("ERROR: try to move "+ to_string(nbToMove) +" in P, but only "+ to_string(nbS) +" S");
            cout << "week " << Parameters::getInstance().weekNum << " WARNING: move only " << nbS << " P instead of " << nbToMove << endl;
            nbToMove = nbS;
        }
      	list_calving_dates lDates;
		lDates = select_and_remove_dates(SS_, nbToMove);
		add_females_with_dates(PP_, lDates);
    }

    void moveSToR(float prop){
        unsigned int nbToMove;
      	list_calving_dates lDates;
        nbToMove = lists[SS_].size() * prop;
		lDates = select_and_remove_dates(SS_, nbToMove);
		add_females_with_dates(RR_, lDates);
    }

	/*! for debug */
	void print_dates_of_list(const int list, string st){
		cout << st << " [" << lists[list].size() << "] ";
		for(int date : lists[list]) cout << date << " ";
		cout << endl;
	}

	void calvings(int calving_date){
		// 1 calving = 1 or 2 animals (if twins)
		// 1 birth = 1 animal
		unsigned int nb_calvings, nb_births;
		int state_calves, list_next_calving, next_calving_date;
        Parameters& iParams = Parameters::getInstance();

		for(int list=0; list<nb_lists; list++){
			// get number of calvings
			nb_calvings = std::count(lists[list].begin(), lists[list].end(), calving_date);
            lists[list].erase(remove(lists[list].begin(), lists[list].end(), calving_date), lists[list].end());
			state_calves = map_list_state_foetus[list];

			// generate next calving dates
			if(nb_calvings > 0){
				list_next_calving = (list==RP_ || list==RR_) ? RM_ : list;
				for(unsigned int q=0; q<nb_calvings; q++){
					next_calving_date = date_generator->get_next_calving_date(calving_date);
					lists[list_next_calving].push_back(next_calving_date);
				}

				// check for twins
				nb_births = nb_calvings + binomial(iParams.twinningRate, nb_calvings);
				Results::get_Instance()->add_data_year(Results::Birth) += nb_births;

				// check for deaths
				// proba of death at birth is not the same if calves are PI or not
				double proba_death = (state_calves == Calves_Group::P_) ?
						iParams.probaOfPDeathAtBirth : iParams.deadCalvesAtBirthProp;
				unsigned int nb_calves_dead = binomial(proba_death, nb_births);
				nb_births -= nb_calves_dead;

				// update of associated Calves group
				if(state_calves == Calves_Group::P_){
					Results::get_Instance()->add_data_year(Results::New_P) += nb_births;
                    Explore::getInstance().add(Explore::eNewP, nb_births);
					calves_group->add_animals(Calves_Group::D_, nb_calves_dead);
				}else{
					calves_group->add_animals(Calves_Group::D_Ca, nb_calves_dead);
				}
				calves_group->decrease_nb_calvings_to_come(nb_calvings);
				calves_group->add_animals(state_calves, nb_births, calving_date);
                if (state_calves == Calves_Group::S_){
                    Explore::getInstance().add(Explore::eNewS, nb_births);
                    Explore::getInstance().addIfPasture(Explore::eNewSAtPasture, nb_births);
                    Explore::getInstance().addByGroup(Explore::eBG_NewS, Explore::eCalves, nb_births);
                    Explore::getInstance().addByGroupIfPasture(Explore::eBG_NewSAtPasture, Explore::eCalves, nb_births);
                }
			}
		}
	}
};

}
