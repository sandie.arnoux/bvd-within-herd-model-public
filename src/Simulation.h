
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "HERD/Herd.h"

namespace model {

class Simulation {
private:
	Herd* herd;
	BVDV_Introduction bvdv;
    vector<unsigned int> vSeeds;

	// to calculate the execution time of some blocks
	int i = 0;
    double start_clock = 0.;
	double end_clock = 0.;
    vector<double> v_comput_times = {0., 0., 0., 0., 0.};
    vector<string> v_names_times = {"transferts entre groupes", "velages", "achats et ventes",
    						        "force de transmission", "transferts entre etats de sante"};

	void stabilization_period();

	void one_repetition();

	void start_the_clock(){
		start_clock = clock();
		i = 0;
	}

	void add_execution_time(){
		end_clock = clock();
		v_comput_times[i] += (end_clock-start_clock);
		start_clock = end_clock;
		i++;
	}

public:
	Simulation();

	virtual ~Simulation();

	BVDV_Introduction& get_BVDV_intro(){
		return bvdv;
	}

	void repetitions();

	void print_execution_time(){
		double total_time=0.;
		for(unsigned int i=0; i<v_comput_times.size(); i++){
			v_comput_times[i] /= CLOCKS_PER_SEC;
			total_time += v_comput_times[i];
			cout << v_names_times[i] << ": " << v_comput_times[i] << endl;
		}
		cout << "total: " << total_time << endl;
	}
};

}
