
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/gamma_distribution.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/triangle_distribution.hpp>
#include <boost/random/binomial_distribution.hpp>
#include <boost/random/bernoulli_distribution.hpp>
#include <chrono>

#include "Parameters.h"

using namespace std;

namespace model {

inline void change_seed(unsigned int& _seed){
    Parameters& iParams = Parameters::getInstance();
    if (_seed == 0)
        _seed = chrono::system_clock::now().time_since_epoch().count();
    iParams.gen.seed(_seed);
}

static boost::random::uniform_real_distribution<> un_01(0.,1.);
inline double uniform_01(){
    Parameters& iParams = Parameters::getInstance();
    return un_01(iParams.gen);
    }

inline int uniform_int(const int min, const int max){
	if (min <= max-1){
        Parameters& iParams = Parameters::getInstance();
		boost::random::uniform_int_distribution<> un_int(min, max-1);
		return un_int(iParams.gen);
	} else {
		return min;
	}
}

inline bool bernoulli(const double p){
    Parameters& iParams = Parameters::getInstance();
    boost::random::bernoulli_distribution<> distribution(p);
    return distribution(iParams.gen);
}

inline int triangular(const int a, const int b, const int c){
    Parameters& iParams = Parameters::getInstance();
	boost::random::triangle_distribution<> distribution(a-1, b, c+1);
	return distribution(iParams.gen);
}

inline int triangular(const int mu, const int delta){
    Parameters& iParams = Parameters::getInstance();
	boost::random::triangle_distribution<> distribution(mu-delta/2-1, mu, mu+delta/2+1);
	return distribution(iParams.gen);
}

inline int binomial(const double p, const unsigned int n){
    Parameters& iParams = Parameters::getInstance();
    boost::random::binomial_distribution<> distribution(n, p);
    return distribution(iParams.gen);
}

inline void multinomial(const int size, const unsigned int N, const double p[], unsigned int n[]){
    int i=0;
    double norm = 0.0;
    double sum_p = 0.0;
    unsigned int sum_n = 0;

    for (i = 0; i < size; i++) norm += p[i];

    for (i = 0; i < size; i++){
    	n[i] = (p[i] > 0.0) ? binomial(p[i] / (norm - sum_p), N - sum_n) : 0;
        sum_p += p[i];
        sum_n += n[i];
    }
}

inline double gaussian(const double mean, const double stddev){
    Parameters& iParams = Parameters::getInstance();
    boost::random::normal_distribution<> distribution(mean, stddev);
    return distribution(iParams.gen);
}

inline double gamma(const double a, const double b){
    Parameters& iParams = Parameters::getInstance();
    boost::random::gamma_distribution<> distribution(a, b);
    return distribution(iParams.gen);
}

}
