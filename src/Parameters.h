
// BVD within-herd model (cow-calf, vaccination)
// =============================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     Arnoux, S., Bidan, F., Damman, A., Petit, E., Assié, S., Ezanno, P., 
//     2021. To Vaccinate or Not: Impact of Bovine Viral Diarrhoea in French 
//     Cow-Calf Herds. Vaccines 9, 1137. https://doi.org/10.3390/vaccines9101137
// 
// 
// License:
// --------
// 
//    Copyright 2012 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include <string>
#include <math.h>
#include <boost/random/mersenne_twister.hpp>

class Parameters {
protected:
	static Parameters *instance;
	Parameters();
	
public:
    int weekNum;
    boost::random::mt19937 gen;
    int daysNbPerWeek;
    int weeksNbPerYear;
    int daysNbPerTimeStep;
    int stabilizationYearsNb;

    double sexRatio;
    int weeksNbBetweenHeifCowsBreedStarts;
    int weeksNbWithoutBreedAfterCalving;
    int weeksNbWithoutBreedAfterAbort;
    int emptyDate;
    int maxFemalesNbPerBull;
    int gestatingDuration;
    double gammaA;
    double gammaB;

    int weeksNbAfterIndoorStartToSellEmpty;
    int weeksNbAfterCalvingEndToPurchaseCalf;

    double probaOfPDeathAtBirth;
    double probaOfPDeathPerYear;
    double mu_P;
    double beta_T;
    double beta_P;
    double beta;
    double rate_M_S;
    double rate_T_R;
    double probaAbortionIfRa;
    double probaAbortionIfRb;
    double probaPbirthIfRb;
    double probaMbirthIfRb;
    double probaRbirthIfRb;
    int lastGestatingWeekNumOfRa;
    int lastGestatingWeekNumOfRb;
    int lastGestatingWeekNumOfRc;

    /************** params changed by the user *************/
    int yearsNb;
    int runsNb;
    bool areTheSeedsToBeRead;
    std::string seedFileName;
    std::string initPopFileName;
    std::string endPopFileName;
    std::string headcountFolder;
    bool isInitializationOfPastureWithRprop;
    std::string pastureInitFileName;
    double femalesRprop;
    bool areHeadcountsToBeSavedInFiles;

    int ageGroupToInfect;
    int ageGroupToProtect;
    int nbToMoveFromSToP;
    float propToMoveFromSToR;

    int renewalRate;
    int breedHeifNb;
    int breedCowsNb;
    double riskFactor;

    std::string weaningDate;
    std::string heifBreedStartDate;
    std::string breedEndDate;
    std::string indoorStartDate;
    std::string pastureStartDate;

    double deadCalvesTotalProp;
    double deadCalvesAfterBirthProp;
    double deadCalvesAtBirthProp;
    double mu_Ca;                   //rate (is computed from deadCalvesAfterBirthProp, which is computed from deadCalvesTotalProp)
    double twinningRate;            //probability !!!
    double heifersFertilityRate;    //probability !!!
    double cowsFertilityRate;       //probability !!!

    double malesSoldAtAutumnProp;
    double malesSoldAtWinterProp;
    double malesSoldAtSpringProp;
    double femalesSoldAtAutumnProp;
    double femalesSoldAtWinterProp;
    double femalesSoldAtSpringProp;

    bool isContactYoungHeifBreedHeif;
    bool isContactYoungHeifCows;
    bool isContactBreedHeifCows;
    bool isContactYoungHeifHerdOutside;
    bool isContactBreedHeifHerdOutside;
    bool isContactCalvesCowsHerdOutside;

    double Kext;
    int KextStartYearNum;

    int vaccinationScenario;
    int vaccStartYearNum;
    int vaccEndYearNum;
    int fullImmunityWeeksNb;
    int partialImmunityWeeksNb;
    double vaccHeifCoverage;
    double vaccCowsCoverage;
    double vaccEfficiency;
    double vaccEfficiencyAgainstSymptoms;
    int primoVaccDoseNb;
    double vaccDoseCost;
    double vaccVetoCost;
    double vaccSubsidiesAmount;
    int realVaccFullImmunityWeeksNb;

    int testCullStartYearNum;
    int testCullEndYearNum;
    int birthToCullWeeksNb;
    double detectionTestSensitivity;

    double maleSoldAtAutumnCost;
    double maleSoldAtWinterCost;
    double maleSoldAtSpringCost;
    double femaleSoldAtAutumnCost;
    double femaleSoldAtWinterCost;
    double femaleSoldAtSpringCost;
    double fatHeiferSaleCost;
    double culledCowSaleCost;
    double calfPurchaseCost;
    double gestCowPurchaseCost;
    double maleSoldAtAutumnWeight;
    double maleSoldAtWinterWeight;
    double maleSoldAtSpringWeight;
    double femaleSoldAtAutumnWeight;
    double femaleSoldAtWinterWeight;
    double femaleSoldAtSpringWeight;
    double fatHeiferSaleWeight;
    double culledCowSaleWeight;
    double calfPurchaseWeight;
    double gestCowPurchaseWeight;
    double animalsCost;
    double totalMeatProd;

	static Parameters& getInstance();
    static void kill();
};
